import matplotlib.pyplot as plt
from pandas import read_csv
from argparse import ArgumentParser
import matplotlib.gridspec as gridspec

parser = ArgumentParser()
parser.add_argument("--domain", type=str)
parser.add_argument("--save",type=bool,default=False)

args = parser.parse_args()

def generatePlotDataLossExplforDomain(domain):
    lossexpl0,lossexpl1=None,None
    normalize=True

    constantvfexpl = None

    cfrplusexpl = None

    if domain=="IIGS5T4":

        constantvfexpl = 7.1
        cfrplusexpl = 0.101

        lossexpl0 = read_csv("experiment_results/LossVsExpl/IIGS5T4_LossesExpl.csv", header=0)
        lossexpl1 = read_csv("experiment_results/LossVsExpl/IIGS5T4_LossesExpl_2.csv", header=0)

        if normalize:
            maxutil = 13
            lossexpl0["expl"] /= maxutil
            lossexpl1["expl"] /= maxutil
            constantvfexpl /= maxutil
            cfrplusexpl /= maxutil

    elif domain=="GP322221":

        constantvfexpl = 2.08169
        cfrplusexpl = 0.01

        lossexpl0 = read_csv("experiment_results/LossVsExpl/GP322221_LossesExpl.csv", header=0)
        lossexpl1 = read_csv("experiment_results/LossVsExpl/GP322221_LossesExpl_2.csv", header=0)

        if normalize:
            maxutil = 37
            lossexpl0["expl"] /= maxutil
            lossexpl1["expl"] /= maxutil
            constantvfexpl /= maxutil
            cfrplusexpl /= maxutil

    elif domain== "OZ8C3B":
        constantvfexpl = 0.69012
        cfrplusexpl = 0.01
        lossexpl0 = read_csv("experiment_results/LossVsExpl/OZ8CB3_LossesExpl_200.csv", header=0)
        lossexpl1 = read_csv("experiment_results/LossVsExpl/OZ8CB3_LossesExpl_20.csv", header=0)

    return [lossexpl0,lossexpl1]

def plotLossExplALL(save):
    domainstrings = ["OZ8C3B", "IIGS5T4", "GP322221"]
    markersize=10
    listofLossEXPLperdomain = []

    for domain in domainstrings:
        listofLossEXPLperdomain.append(generatePlotDataLossExplforDomain(domain))

    lossexpl0oz = listofLossEXPLperdomain[0][0]
    lossexpl1oz = listofLossEXPLperdomain[0][1]

    lossexpl0gs = listofLossEXPLperdomain[1][0]
    lossexpl1gs = listofLossEXPLperdomain[1][1]

    lossexpl0lh = listofLossEXPLperdomain[2][0]
    lossexpl1lh = listofLossEXPLperdomain[2][1]

    gs = gridspec.GridSpec(3, 3)

    gs.update(wspace=0.1, hspace=0.3)
    fig = plt.figure()

    # fig, axes = plt.subplots(2, 2, figsize=(20, 6))
    # fig.subplots_adjust(top=0.99,right=0.99,left=0.1,bottom=0.1)
    plt.rcParams['ytick.labelsize'] = 20
    plt.rcParams['xtick.labelsize'] = 20
    # gs

    axgshuber = fig.add_subplot(gs[0,0])
    axgshuber.plot(lossexpl0gs["huber"], lossexpl0gs["expl"], ".",markersize=markersize)
    axgshuber.plot(lossexpl1gs["huber"], lossexpl1gs["expl"], ".",markersize=markersize)

    # axgshuber.set_ylabel("exploitability", fontsize=40)
    # axgshuber.set_xlabel("Huber", fontsize=40)
    # if domain=="OZ8C3B":
    #     axes[0].ticklabel_format(style='sci', axis='x', scilimits=(0,0))

    # axes[0].axhline(y=constantvfexpl,color="red")
    cfrplusexplgs = 0.0122
    axgshuber.axhline(y=cfrplusexplgs,color="green",lw=2)



    axgsl1 = fig.add_subplot(gs[0, 1])
    axgsl1.plot(lossexpl0gs["l1"], lossexpl0gs["expl"], ".",markersize=markersize)
    axgsl1.plot(lossexpl1gs["l1"], lossexpl1gs["expl"], ".",markersize=markersize)
    # axgsl1.set_xlabel("L-1", fontsize=40)
    axgsl1.set_yticks([])
    axgsl1.set_title("GS", fontsize=20)


    axgslinf = fig.add_subplot(gs[0, 2])
    axgslinf.plot(lossexpl0gs["linf"], lossexpl0gs["expl"], ".",markersize=markersize)
    axgslinf.plot(lossexpl1gs["linf"], lossexpl1gs["expl"], ".",markersize=markersize)
    # axgslinf.set_xlabel("L-Infinity", fontsize=40)
    axgslinf.set_yticks([])

    # axes[2].axhline(y=constantvfexpl,color="red")
    # axgslinf.axhline(y=cfrplusexpl,color="green",lw=5)

    # oz

    axozhuber = fig.add_subplot(gs[1,0])
    axozhuber.plot(lossexpl0oz["huber"], lossexpl0oz["expl"], ".",markersize=markersize)
    axozhuber.plot(lossexpl1oz["huber"], lossexpl1oz["expl"], ".",markersize=markersize)

    axozhuber.set_ylabel("exploitability", fontsize=40)

    axozhuber.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
    # axozhuber.set_xlabel("Huber", fontsize=40)
    # if domain=="OZ8C3B":
    #     axes[0].ticklabel_format(style='sci', axis='x', scilimits=(0,0))

    # axes[0].axhline(y=constantvfexpl,color="red")

    axozhuber.axhline(y=0.01, color="green", lw=2)


    axozl1 = fig.add_subplot(gs[1, 1])
    axozl1.plot(lossexpl0oz["l1"], lossexpl0oz["expl"], ".",markersize=markersize)
    axozl1.plot(lossexpl1oz["l1"], lossexpl1oz["expl"], ".",markersize=markersize)
    # axozl1.set_xlabel("L-1", fontsize=40)
    axozl1.set_yticks([])

    axozl1.set_title("OZ", fontsize=20)


    axozlinf = fig.add_subplot(gs[1, 2])
    axozlinf.plot(lossexpl0oz["linf"], lossexpl0oz["expl"], ".",markersize=markersize)
    axozlinf.plot(lossexpl1oz["linf"], lossexpl1oz["expl"], ".",markersize=markersize)
    # axozlinf.set_xlabel("L-Infinity", fontsize=40)
    axozlinf.set_yticks([])

    # lh

    axlhhuber = fig.add_subplot(gs[2,0])
    axlhhuber.plot(lossexpl0lh["huber"], lossexpl0lh["expl"], ".",markersize=markersize)
    axlhhuber.plot(lossexpl1lh["huber"], lossexpl1lh["expl"], ".",markersize=markersize)

    # axlhhuber.set_ylabel("exploitability", fontsize=40)
    axlhhuber.set_xlabel("Huber", fontsize=40)
    # if domain=="OZ8C3B":
    #     axes[0].ticklabel_format(style='sci', axis='x', scilimits=(0,0))

    # axes[0].axhline(y=constantvfexpl,color="red")

    axlhhuber.axhline(y=0.01, color="green", lw=2)


    axlhl1 = fig.add_subplot(gs[2, 1])
    axlhl1.plot(lossexpl0lh["l1"], lossexpl0lh["expl"], ".",markersize=markersize)
    axlhl1.plot(lossexpl1lh["l1"], lossexpl1lh["expl"], ".",markersize=markersize)
    axlhl1.set_xlabel("L-1", fontsize=40)
    axlhl1.set_yticks([])

    axlhl1.set_title("LH", fontsize=20)


    axlhlinf = fig.add_subplot(gs[2, 2])
    axlhlinf.plot(lossexpl0lh["linf"], lossexpl0lh["expl"], ".",markersize=markersize)
    axlhlinf.plot(lossexpl1lh["linf"], lossexpl1lh["expl"], ".",markersize=markersize)
    axlhlinf.set_xlabel("L-Infinity", fontsize=40)
    axlhlinf.set_yticks([])


    # axes[1][1].legend(["Huber","L1","L-Infinity"],prop={"size": 30})
    # if domain=="GP322221" or domain=="OZ8C3B":
    # 	plt.xticks()
    # else:
    # 	plt.xticks(x)
    # plt.tick_params(labelsize=30)
    # fig.tight_layout(pad=1.0)

    # stackoverflow
    # plt.gca().set_axis_off()
    # plt.subplots_adjust(top=1, bottom=0, right=1, left=0,
    # 					hspace=0, wspace=0)
    # plt.margins(0, 0)
    # plt.gca().xaxis.set_major_locator(plt.NullLocator())
    # plt.gca().yaxis.set_major_locator(plt.NullLocator())
    # plt.savefig("filename.pdf", bbox_inches='tight',
    # 			pad_inches=0)

    if save:
        plt.savefig("experiment_results/LossVsExpl/ALLLossexl.pdf", bbox_inches='tight',
                    pad_inches=0)
    # gs.tight_layout(fig,h_pad=0)
    # gs.show()g
    fig.tight_layout()
    plt.show()


def plotLossesExplFinalDomain(domain,save):

    lossexpl0,lossexpl1=None,None
    normalize=True

    constantvfexpl = None

    cfrplusexpl = None

    if domain=="IIGS5T4":

        constantvfexpl = 7.1
        cfrplusexpl = 0.101

        lossexpl0 = read_csv("experiment_results/LossVsExpl/IIGS5T4_LossesExpl.csv", header=0)
        lossexpl1 = read_csv("experiment_results/LossVsExpl/IIGS5T4_LossesExpl_2.csv", header=0)

        if normalize:
            maxutil = 13
            lossexpl0["expl"] /= maxutil
            lossexpl1["expl"] /= maxutil
            constantvfexpl /= maxutil
            cfrplusexpl /= maxutil

    elif domain=="GP322221":

        constantvfexpl = 2.08169
        cfrplusexpl = 0.01

        lossexpl0 = read_csv("experiment_results/LossVsExpl/GP322221_LossesExpl.csv", header=0)
        lossexpl1 = read_csv("experiment_results/LossVsExpl/GP322221_LossesExpl_2.csv", header=0)

        if normalize:
            maxutil = 37
            lossexpl0["expl"] /= maxutil
            lossexpl1["expl"] /= maxutil
            constantvfexpl /= maxutil
            cfrplusexpl /= maxutil

    elif domain== "OZ8C3B":
        constantvfexpl = 0.69012
        cfrplusexpl = 0.01
        lossexpl0 = read_csv("experiment_results/LossVsExpl/OZ8CB3_LossesExpl_200.csv", header=0)
        lossexpl1 = read_csv("experiment_results/LossVsExpl/OZ8CB3_LossesExpl_20.csv", header=0)


    plt.rcParams['ytick.labelsize'] = 40
    plt.rcParams['xtick.labelsize'] = 40
    fig, axes = plt.subplots(1,3,figsize=(20,6))
    fig.subplots_adjust(wspace=0.05,top=0.99,right=0.99)
    # fig.set_size_inches(10,10)


    axes[0].plot(lossexpl0["huber"], lossexpl0["expl"], ".",markersize=10)
    axes[0].plot(lossexpl1["huber"], lossexpl1["expl"], ".",markersize=10)

    axes[0].set_ylabel("exploitability", fontsize=40)
    axes[0].set_xlabel("Huber", fontsize=40)
    if domain=="OZ8C3B":
        axes[0].ticklabel_format(style='sci', axis='x', scilimits=(0,0))

    # axes[0].axhline(y=constantvfexpl,color="red")
    axes[0].axhline(y=cfrplusexpl,color="green",lw=5)


    axes[1].plot(lossexpl0["l1"], lossexpl0["expl"], ".",markersize=10)
    axes[1].plot(lossexpl1["l1"], lossexpl1["expl"], ".",markersize=10)
    axes[1].set_xlabel("L-1", fontsize=40)
    axes[1].set_yticks([])
    # axes[1].axhline(y=constantvfexpl,color="red")
    axes[1].axhline(y=cfrplusexpl,color="green",lw=5)


    axes[2].plot(lossexpl0["linf"], lossexpl0["expl"], ".",markersize=10)
    axes[2].plot(lossexpl1["linf"], lossexpl1["expl"], ".",markersize=10)
    axes[2].set_xlabel("L-Infinity", fontsize=40)
    axes[2].set_yticks([])
    # axes[2].axhline(y=constantvfexpl,color="red")
    axes[2].axhline(y=cfrplusexpl,color="green",lw=5)
    # axes[2].legend(["DL-CFR+NN1","DL-CFR+NN2","CFR+"],prop={"size":18})


    if save:
        plt.savefig("experiment_results/lossvsexpl_plots/" + domain + "_lossvsexpl.pdf", bbox_inches='tight',
                    pad_inches=0)
    plt.tight_layout()
    plt.show()

def main(args):
    plotLossExplALL(args.save)

if __name__ == "__main__":
    main(args)
