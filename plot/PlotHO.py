import matplotlib.pyplot as plt
from pandas import read_csv
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("--domain", type=str)
parser.add_argument("--experiment", type=str)

args = parser.parse_args()

def plotHo(pathtocsvs,numfiles,savepath):
	cols = ['epoch', 'huber_loss', 'l0_loss', 'l1_loss', 'linf_loss', 'loss',
	        'val_huber_loss', 'val_l0_loss', 'val_l1_loss', 'val_linf_loss',
	        'val_loss']

	dflist = []

	for i in range(1,numfiles+1):
		df = read_csv(pathtocsvs+"_"+str(i)+".csv")
		dflist.append(df)

	x = dflist[0]["epoch"]

	plt.subplot(1, 3, 1)
	for i in range(numfiles):
		plt.plot(x, dflist[i]["huber_loss"])

	plt.title("Huber")
	plt.ylabel("huber")
	plt.xlabel("epoch")
	plt.yscale("log")
	plt.legend([str(i) for i in range(1,numfiles+1)])

	plt.subplot(1, 3, 2)
	for i in range(numfiles):
		plt.plot(x, dflist[i]["l1_loss"])

	plt.title("L1")
	plt.ylabel("l1")
	plt.xlabel("epoch")
	plt.yscale("log")
	plt.legend([str(i) for i in range(1, numfiles + 1)])

	plt.subplot(1, 3, 3)
	for i in range(numfiles):
		plt.plot(x, dflist[i]["linf_loss"])

	plt.title("Linf")
	plt.ylabel("linf")
	plt.xlabel("epoch")
	plt.yscale("log")
	plt.legend([str(i) for i in range(1, numfiles + 1)])

	plt.show()

	if savepath is not None:
		plt.savefig(savepath)

def plotHoBest(pathtocsvs,numfiles,savepath,experimenttype,domain,log=True):
	cols = ['epoch', 'huber_loss', 'l0_loss', 'l1_loss', 'linf_loss', 'loss',
	        'val_huber_loss', 'val_l0_loss', 'val_l1_loss', 'val_linf_loss',
	        'val_loss']

	dflist = []

	if experimenttype=="data":
		plottitle= "Data Amount"
		xlabel = "Number of training samples in thousands"
		mylist = [i for i in range(1000, numfiles * 1000 + 1000, 1000)]
		for i in mylist:
			df = read_csv(pathtocsvs + "_" + str(i) + ".csv", error_bad_lines=False)
			dflist.append(df)

	else:
		for i in range(1,numfiles+1,1):
			df = read_csv(pathtocsvs+"_"+str(i)+".csv",error_bad_lines=False)
			dflist.append(df)

	besthuberpersetting = []
	bestl1persetting = []
	bestlinfpersetting = []

	plt.subplots_adjust(right=0.99)



	for i in range(numfiles):
		besthuberpersetting.append(min(dflist[i]["val_huber_loss"]))
		bestl1persetting.append(min(dflist[i]["val_l1_loss"]))
		bestlinfpersetting.append(min(dflist[i]["val_linf_loss"]))

	if experimenttype == "depth":
		besthuberpersetting = sorted(besthuberpersetting,reverse=True)
		bestl1persetting = sorted(bestl1persetting,reverse=True)
		bestlinfpersetting = sorted(bestlinfpersetting,reverse=True)


	plt.subplot(1, 3, 1)
	for i in range(numfiles):
		plt.plot(range(1,numfiles+1), besthuberpersetting)

	plt.title("Huber",fontsize=40)
	plt.ylabel("Loss",fontsize=40)
	plt.xlabel("")
	plt.tick_params(labelsize=20)
	if log:
		plt.yscale("log")

	plt.subplot(1, 3, 2)
	for i in range(numfiles):
		plt.plot(range(1,numfiles+1), bestl1persetting)

	plt.title("L1",fontsize=40)
	plt.ylabel("")
	plt.xlabel(experimenttype,fontsize=40)
	plt.tick_params(labelsize=20)
	if log:
		plt.yscale("log")

	plt.subplot(1, 3, 3)
	for i in range(numfiles):
		plt.plot(range(1,numfiles+1), bestlinfpersetting)

	plt.title("Linf",fontsize=40)
	plt.ylabel("")
	plt.xlabel("")
	plt.tick_params(labelsize=20)
	if log:
		plt.yscale("log")

	plt.suptitle(domain,fontsize=40)
	plt.show()
	if savepath is not None:

		plt.savefig(savepath,format="pdf")

def plotBestOnePlot(pathtocsvs,numfiles,savepath,experimenttype,domain,log=True):
	cols = ['epoch', 'huber_loss', 'l0_loss', 'l1_loss', 'linf_loss', 'loss',
	        'val_huber_loss', 'val_l0_loss', 'val_l1_loss', 'val_linf_loss',
	        'val_loss']

	dflist = []

	for i in range(1,numfiles+1):
		df = read_csv(pathtocsvs+"_"+str(i)+".csv")
		dflist.append(df)

	besthuberpersetting = []
	bestl1persetting = []
	bestlinfpersetting = []

	for i in range(numfiles):
		besthuberpersetting.append(min(dflist[i]["huber_loss"]))
		bestl1persetting.append(min(dflist[i]["l1_loss"]))
		bestlinfpersetting.append(min(dflist[i]["linf_loss"]))

	plt.title(domain)

	x=range(1,numfiles+1)

	for i in range(numfiles):
		plt.plot(x, besthuberpersetting)
		plt.plot(x, bestl1persetting)
		plt.plot(x, bestlinfpersetting)

	plt.show()
	if savepath is not None:

		plt.savefig(savepath)

def plotHoDifferentLosses(domain):
	pathtocsvs = None
	if domain=="GP322221":
		pathtocsvs="hyp_opt/Loss/GP322221_HO"
	elif domain=="IIGS5T4":
		pathtocsvs="hyp_opt/Loss/IIGS5T4_HO"
	elif domain == "OZ8C3B":
		pathtocsvs = "hyp_opt/Loss/OZ8C3B_HO"

	cols = ['epoch', 'huber_loss', 'l0_loss', 'l1_loss', 'linf_loss', 'loss',
	        'val_huber_loss', 'val_l0_loss', 'val_l1_loss', 'val_linf_loss',
	        'val_loss']

	huber = read_csv(pathtocsvs+"_huber.csv")
	l1 = read_csv(pathtocsvs + "_l1.csv")
	linf = read_csv(pathtocsvs + "_linf.csv")

	x = huber["epoch"]

	plt.subplot(1, 3, 1)
	plt.plot(x, huber["huber_loss"])
	plt.plot(x, huber["val_huber_loss"])
	plt.plot(x, huber["val_l1_loss"])
	plt.plot(x, huber["val_linf_loss"])
	plt.title("Minimize Huber",fontsize=22)
	plt.ylabel("Loss",fontsize=22)
	plt.xlabel("")
	plt.tick_params(labelsize=18)
	plt.yscale("log")
	plt.legend(["train_huber_loss","val_huber_loss","val_l1_loss","val_linf_loss"])
	plt.legend(prop={"size": 18})

	plt.subplot(1, 3, 2)
	plt.plot(x, l1["l1_loss"])
	plt.plot(x, l1["val_l1_loss"])
	plt.plot(x, l1["val_huber_loss"])
	plt.plot(x, l1["val_linf_loss"])
	plt.tick_params(labelsize=18)
	plt.title("Minimize L1",fontsize=22)
	plt.ylabel("")
	plt.xlabel("Epochs",fontsize=22)
	plt.yscale("log")
	plt.legend(["train_l1_loss","val_l1_loss","val_huber_loss","val_linf_loss"])
	plt.legend(prop={"size": 18})

	plt.subplot(1, 3, 3)
	plt.plot(x, linf["linf_loss"])
	plt.plot(x, linf["val_linf_loss"])
	plt.plot(x, linf["val_huber_loss"])
	plt.plot(x, linf["val_l1_loss"])
	plt.tick_params(labelsize=18)
	plt.title("Minimize Linf",fontsize=22)
	plt.ylabel("")
	plt.xlabel("")
	plt.yscale("log")
	plt.legend(["train_linf_loss","val_linf_loss","val_huber_loss","val_l1_loss"])
	plt.legend(prop={"size": 18})
	plt.show()

def plotValCFRDlosses(domain):
	cfrd_losses = None

	if domain == "IIGS5T4":
		cfrd_losses = read_csv("experiment_results/val_cfrd/IIGS5T4_cfrdlosses.csv")

	elif domain == "OZ8C3B":
		cfrd_losses = read_csv("experiment_results/val_cfrd/OZ8C3B_cfrdlosses.csv")

	elif domain == "GP322221":
		cfrd_losses = read_csv("experiment_results/val_cfrd/GP322221_cfrdlosses.csv")

	x = cfrd_losses["epoch"]
	plt.title(domain + " CFRD validation losses",fontsize=40)
	plt.plot(x, cfrd_losses["huber_loss"])
	plt.plot(x, cfrd_losses["val_huber_loss"])
	plt.plot(x, cfrd_losses["val_l1_loss"])
	plt.plot(x, cfrd_losses["val_linf_loss"])
	plt.legend(["Huber on Random Ranges","Val Huber on CFRD","Val L1 on CFRD","Val Linf on CFRD"],prop={"size": 18})
	# plt.yscale("log")
	plt.ylabel("Loss",fontsize=40)
	plt.xlabel("Epoch",fontsize=40)
	plt.tick_params(labelsize=35)
	plt.legend(prop={"size": 25},loc="best")
	plt.show()


def plotDomainExperiment(domain,experiment):

	if domain == "IIGS5T4":

		if experiment == "width":
			plotHoBest("hyp_opt/Width/IIGS5T4_HO_Width", 7, None,experiment,domain, False)

		elif experiment == "depth":
			plotHoBest("hyp_opt/Depth/IIGS5T4_HO_Nhidden", 9, None,experiment,domain, False)

		elif experiment == "data":
			plotHoBest("hyp_opt/DataAmount/IIGS5T4_HO_DataAmount", 41, None,experiment,domain, True)

		elif experiment == "loss":
			plotHoDifferentLosses(domain)

		elif experiment == "cfrdlosses":
			plotValCFRDlosses(domain)


	elif domain == "OZ8C3B":

		if experiment == "width":
			plotHoBest("hyp_opt/Width/OZ8C3B_HO_Width", 7, None,experiment,domain, False)

		elif experiment == "depth":
			plotHoBest("hyp_opt/Depth/OZ8C3B_HO_Nhidden", 9, None,experiment,domain, False)

		elif experiment == "data":
			plotHoBest("hyp_opt/DataAmount/OZ8C3B_HO_DataAmount", 25, None,experiment,domain, True)

		elif experiment == "loss":
			plotHoDifferentLosses(domain)

		elif experiment == "cfrdlosses":
			plotValCFRDlosses(domain)

	elif domain == "GP322221":

		if experiment == "width":
			plotHoBest("hyp_opt/Width/GP322221_HO_Width", 19, None,experiment,domain, False)

		elif experiment == "depth":
			plotHoBest("hyp_opt/Depth/GP322221_HO_Nhidden", 9, None,experiment,domain, False)

		elif experiment == "data":
			plotHoBest("hyp_opt/DataAmount/GP322221_HO_DataAmount", 38, None,experiment,domain, True)

		elif experiment == "loss":
			plotHoDifferentLosses(domain)

		elif experiment == "cfrdlosses":
			plotValCFRDlosses(domain)

def main(args):
	plotDomainExperiment(args.domain,args.experiment)

if __name__ == "__main__":
	main(args)

