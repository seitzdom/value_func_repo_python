import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib import ticker
from argparse import ArgumentParser

parser= ArgumentParser()
parser.add_argument("--domain",type=str)
parser.add_argument("--ps_idx",type=int)
parser.add_argument("--save",default=False,type=bool)
args = parser.parse_args()

def plotHeatmapAllDomains(domain,ps_idx,save=False):

	numPubStateFeatures = None
	numPubstates = None
	iter_num = None
	cfrd = None
	random = None
	cfrnn = None

	if domain=="OZ8C3B":
		numPubStateFeatures = 7
		numPubstates = 17
		iter_num = 10
		cfrd = pd.read_csv(
			"heatmap_data/OZ8C3B/input_osh_c8l1_trunk6_cfrd.csv")
		random = pd.read_csv(
			"data/data_OZ8CB3/input_osh_c8l1_trunk6_2000.csv")
		cfrnn = pd.read_csv(
			"heatmap_data/OZ8C3B/input_osh_c8l1_trunk6_cfrnn.csv")

	elif domain == "IIGS5T4":
		cfrd = pd.read_csv(
			"heatmap_data/IIGS5T4/cfrd_data/IIGS5T4_cfrd_input.csv")
		random = pd.read_csv("data/data_iigs5t4/input_gs5_trunk4.csv")
		cfrnn = pd.read_csv(
			"heatmap_data/IIGS5T4/cfrnn_data/IIGS5T4_cfrnn_input.csv")
		numPubStateFeatures = 5
		numPubstates = 9
		iter_num = 20

	elif domain == "GP322221":
		cfrd = pd.read_csv(
			"heatmap_data/GP322221/cfrd_data/GP322221_cfrd_input.csv")
		random = pd.read_csv(
			"data/data_poker/input_poker.csv")
		cfrnn = pd.read_csv(
			"heatmap_data/GP322221/cfrnn_data/GP322221_cfrnn_input.csv")
		numPubStateFeatures = 27
		numPubstates = 87
		iter_num = 10

	fix_range = random.values[ps_idx:iter_num * numPubstates:numPubstates, numPubStateFeatures:]
	cfrd_range = cfrd.values[ps_idx:iter_num * numPubstates:numPubstates, numPubStateFeatures:]
	cfrnn_range = cfrnn.values[ps_idx:iter_num * numPubstates:numPubstates, numPubStateFeatures:]

	f, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(20,6),sharey="all",sharex="all")
	f.subplots_adjust(wspace=0.1,top=0.98,right=0.97,left=0.06,bottom=0.1)
	sns.set(font_scale=1.4)
	yticks = ticker.MaxNLocator(4)
	ax1.yaxis.set_major_locator(yticks)

	g1 = sns.heatmap(cfrd_range, cmap='Greys', vmin=0, vmax=1,ax=ax1,cbar=False)
	g1.set_ylabel("Iteration",fontsize=40)
	g1.set_yticks([])
	g1.set_xlabel("CFR-D",fontsize=40)

	ax1.tick_params(labelsize=32,labelrotation=0.5)
	ax1.set_yticks([i for i in range(0,iter_num,5)],minor=True)

	g2 = sns.heatmap(cfrnn_range, cmap='Greys', vmin=0, vmax=1, ax=ax2,cbar=False)
	g2.set_xlabel("DL-CFR+NN",fontsize=40)
	g2.set_ylabel("")
	g2.set_yticks([])
	ax2.tick_params(labelsize=40, labelrotation=0.25)

	g3 = sns.heatmap(fix_range, cmap='Greys', vmin=0, vmax=1, ax=ax3)
	g3.set_xlabel("Random",fontsize=40)
	g3.set_ylabel("")
	ax3.tick_params(labelsize=40, labelrotation=0.5)

	cax = plt.gcf().axes[-1]
	cax.tick_params(labelsize=40)

	if save:
		plt.savefig("experiment_results/heatmap_plots/"+domain+"_heatmaps.pdf", bbox_inches='tight',
	        pad_inches=0)
	plt.tight_layout()
	plt.show()

def main(args):
	plotHeatmapAllDomains(args.domain,args.ps_idx,args.save)

if __name__ == "__main__":
	main(args)