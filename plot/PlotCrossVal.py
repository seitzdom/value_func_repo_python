import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from pandas import read_csv
import numpy as np
from argparse import ArgumentParser
from matplotlib.ticker import FuncFormatter

import matplotlib.pylab as pl
import matplotlib.gridspec as gridspec



parser = ArgumentParser()
parser.add_argument("--domain", type=str)
parser.add_argument("--save",default=False,type=bool)
args = parser.parse_args()

inf_string = u"\u221E"

def my_formatter(x, pos):
    """Format 1 as 1, 0 as 0, and all values whose absolute values is between
    0 and 1 without the leading "0." (e.g., 0.7 is formatted as .7 and -0.4 is
    formatted as -.4)."""
    val_str = '{:g}'.format(x)
    if np.abs(x) > 0 and np.abs(x) < 1:
        return val_str.replace("0", "", 1)
    else:
        return val_str

major_formatter = FuncFormatter(my_formatter)

def genPlotCrossvalDataforDomain(domain):
	numPubStates = None
	dflist = []
	path = None
	x = None
	if domain=="OZ8C3B":
		path = "experiment_results/publicstate_crossval/OZ8C3B_publicstate"
		numPubStates = 17

	elif domain == "IIGS5T4":
		path = "experiment_results/publicstate_crossval/IIGS5T4_publicstate"
		numPubStates = 9

	elif domain == "GP322221":
		path = "experiment_results/publicstate_crossval/GP322221_publicstate"
		numPubStates = 87

	for i in range(numPubStates):
		df = read_csv(path+"_"+str(i)+"_losses.csv",header=0)
		dflist.append(df)

	besthuberperPubState = []
	bestl1perPubState = []
	bestlinfperPubState = []

	x = np.arange(numPubStates)

	for i in range(numPubStates):
		besthuberperPubState.append(min(dflist[i]["val_huber_loss"]))
		bestl1perPubState.append(min(dflist[i]["val_l1_loss"]))
		bestlinfperPubState.append(min(dflist[i]["val_linf_loss"]))

	return [besthuberperPubState,bestl1perPubState,bestlinfperPubState]



def plotCrossValAllDomains3subfigs(save):

	xoz,xgs,xlh = np.arange(17),np.arange(9),np.arange(87)

	domainstrings = ["OZ8C3B","IIGS5T4","GP322221"]

	listoflossesperdomain = []

	for domain in domainstrings:
		listoflossesperdomain.append(genPlotCrossvalDataforDomain(domain))

	ozdata = listoflossesperdomain[0]
	gsdata = listoflossesperdomain[1]
	lhdata = listoflossesperdomain[2]

	ozhuber = ozdata[0]
	ozl1 = ozdata[1]
	ozlinf = ozdata[2]

	gshuber = gsdata[0]
	gsl1 = gsdata[1]
	gslinf = gsdata[2]

	lhhuber = lhdata[0]
	lhl1 = lhdata[1]
	lhlinf = lhdata[2]

	# Create 2x2 sub plots
	gs = gridspec.GridSpec(2, 2)

	gs.update(wspace=0.1, hspace=0.1)
	fig = plt.figure()

	# fig, axes = plt.subplots(2, 2, figsize=(20, 6))
	# fig.subplots_adjust(top=0.99,right=0.99,left=0.1,bottom=0.1)
	plt.rcParams['ytick.labelsize'] = 20
	plt.rcParams['xtick.labelsize'] = 20
	# oz
	axoz = fig.add_subplot(gs[0,0])
	axoz.bar(xoz -0.2, ozhuber, width=0.2,align='center',label="Val_Huber")
	axoz.bar(xoz, ozl1, width=0.2, align='center',label="Val_L1")
	axoz.bar(xoz +0.2, ozlinf, width=0.2, align='center',label="Val_Linf")
	# axes[0][0].legend(prop={"size":30})
	axoz.xaxis.set_major_locator(MaxNLocator(integer=True))
	axoz.set_title("OZ",fontsize=20)

	plt.yscale("log")
	plt.ylabel("Loss",fontsize=35)
	# plt.xlabel("Index of public state",fontsize=35)

	axgs = fig.add_subplot(gs[0, 1])
	axgs.bar(xgs -0.2, gshuber, width=0.2,align='center',label="Val_Huber")
	axgs.bar(xgs, gsl1, width=0.2, align='center',label="Val_L1")
	axgs.bar(xgs +0.2, gslinf, width=0.2, align='center',label="Val_Linf")
	axgs.legend(prop={"size":15})
	axgs.xaxis.set_major_locator(MaxNLocator(integer=True))
	axgs.set_title("GS",fontsize=20)
	plt.yscale("log")
	# plt.ylabel("Loss",fontsize=35)
	# plt.xlabel("Index of public state",fontsize=35)

	axlh = fig.add_subplot(gs[1,:])
	axlh.bar(xlh -0.2, lhhuber, width=1,align='center',label="Val_Huber")
	axlh.bar(xlh, lhl1, width=0.5, align='center',label="Val_L1")
	axlh.bar(xlh +0.2, lhlinf, width=0.5, align='center',label="Val_Linf")
	axlh.xaxis.set_major_locator(MaxNLocator(integer=True))
	axlh.set_title("LH",fontsize=20)
	plt.yscale("log")
	# plt.ylabel("Loss",fontsize=35)
	plt.xlabel("Index of public state",fontsize=35)

	# axes[1][1].legend(["Huber","L1","L-Infinity"],prop={"size": 30})
	# if domain=="GP322221" or domain=="OZ8C3B":
	# 	plt.xticks()
	# else:
	# 	plt.xticks(x)
	# plt.tick_params(labelsize=30)
	# fig.tight_layout(pad=1.0)


    # stackoverflow
	# plt.gca().set_axis_off()
	# plt.subplots_adjust(top=1, bottom=0, right=1, left=0,
	# 					hspace=0, wspace=0)
	# plt.margins(0, 0)
	# plt.gca().xaxis.set_major_locator(plt.NullLocator())
	# plt.gca().yaxis.set_major_locator(plt.NullLocator())
	# plt.savefig("filename.pdf", bbox_inches='tight',
	# 			pad_inches=0)

	if save:
		plt.savefig("experiment_results/pubstatecrossval_plots/ALLpubstatecrossval.pdf", bbox_inches='tight',
	        pad_inches=0)
	# gs.tight_layout(fig,h_pad=0)
	# gs.show()g
	fig.tight_layout()
	plt.show()

def plotCrossValAllDomains(domain,save):
	numPubStates = None
	dflist = []
	path = None
	x = None
	if domain=="OZ8C3B":
		path = "experiment_results/publicstate_crossval/OZ8C3B_publicstate"
		numPubStates = 17

	elif domain == "IIGS5T4":
		path = "experiment_results/publicstate_crossval/IIGS5T4_publicstate"
		numPubStates = 9

	elif domain == "GP322221":
		path = "experiment_results/publicstate_crossval/GP322221_publicstate"
		numPubStates = 87

	for i in range(numPubStates):
		df = read_csv(path+"_"+str(i)+"_losses.csv",header=0)
		dflist.append(df)

	besthuberperPubState = []
	bestl1perPubState = []
	bestlinfperPubState = []

	x = np.arange(numPubStates)

	for i in range(numPubStates):
		besthuberperPubState.append(min(dflist[i]["val_huber_loss"]))
		bestl1perPubState.append(min(dflist[i]["val_l1_loss"]))
		bestlinfperPubState.append(min(dflist[i]["val_linf_loss"]))
	plt.subplots_adjust(top=0.99,right=0.99,left=0.1,bottom=0.1)
	ax = plt.subplot()
	ax.bar(x -0.2, besthuberperPubState, width=0.2,align='center',label="Val_Huber")
	ax.bar(x, bestl1perPubState, width=0.2, align='center',label="Val_L1")
	ax.bar(x +0.2, bestlinfperPubState, width=0.2, align='center',label="Val_Linf")
	ax.legend(prop={"size":30})
	ax.xaxis.set_major_locator(MaxNLocator(integer=True))
	plt.yscale("log")
	plt.ylabel("Loss",fontsize=35)
	plt.xlabel("Index of public state",fontsize=35)
	if domain=="GP322221" or domain=="OZ8C3B":
		plt.xticks()
	else:
		plt.xticks(x)
	plt.tick_params(labelsize=30)
	if save:
		plt.savefig("experiment_results/pubstatecrossval_plots/"+domain+"_pubstatecrossval.pdf", bbox_inches='tight',
	        pad_inches=0)
	plt.show()

def plotCrossValAllDomainsOneLoss(domain,save):
	numPubStates = None
	dflist = []
	path = None
	x = None
	if domain=="OZ8C3B":
		path = "experiment_results/publicstate_crossval/OZ8C3B_publicstate"
		numPubStates = 17

	elif domain == "IIGS5T4":
		path = "experiment_results/publicstate_crossval/IIGS5T4_publicstate"
		numPubStates = 9

	elif domain == "GP322221":
		path = "experiment_results/publicstate_crossval/GP322221_publicstate"
		numPubStates = 87

	for i in range(numPubStates):
		df = read_csv(path+"_"+str(i)+"_losses.csv",header=0)
		dflist.append(df)

	besthuberperPubState = []
	bestl1perPubState = []
	bestlinfperPubState = []

	x = np.arange(numPubStates)

	for i in range(numPubStates):
		besthuberperPubState.append(min(dflist[i]["val_huber_loss"]))
		bestl1perPubState.append(min(dflist[i]["val_l1_loss"]))
		bestlinfperPubState.append(min(dflist[i]["val_linf_loss"]))
	plt.subplots_adjust(top=0.99,right=0.99,left=0.11,bottom=0.1)
	ax = plt.subplot()
	# ax.bar(x -0.2, besthuberperPubState, width=0.2,align='center',label="Val_Huber")
	ax.bar(x, sorted(bestl1perPubState), align='center',label="Val_L1")
	# ax.bar(x +0.2, bestlinfperPubState, width=0.2, align='center',label="Val_Linf")
	ax.legend(prop={"size":35})
	ax.xaxis.set_major_locator(MaxNLocator(integer=True))
	# plt.yscale("log")
	plt.ylabel("Loss",fontsize=40)
	plt.xlabel("Index of public state",fontsize=40)
	if domain=="GP322221" or domain=="OZ8C3B":
		plt.xticks()
	else:
		plt.xticks(x)
	plt.tick_params(labelsize=40)
	if save:
		plt.savefig("experiment_results/pubstatecrossval_plots/"+domain+"_pubstatecrossval.pdf", bbox_inches='tight',
	        pad_inches=0)
	# plt.tight_layout()
	plt.show()

def calcCorCrossValNPubstates():

	dflistOZ = []
	dflistIIGS = []
	dflistGP = []
	path = None
	x = None

	pathOZ = "experiment_results/publicstate_crossval/OZ8C3B_publicstate"
	numPubStatesOZ = 17


	pathIIGS = "experiment_results/publicstate_crossval/IIGS5T4_publicstate"
	numPubStatesIIGS = 9


	pathGP = "experiment_results/publicstate_crossval/GP322221_publicstate"
	numPubStatesGP = 87

	for i in range(numPubStatesOZ):
		df = read_csv(pathOZ+"_"+str(i)+"_losses.csv",header=0)
		dflistOZ.append(df)

	for i in range(numPubStatesIIGS):
		df = read_csv(pathIIGS+"_"+str(i)+"_losses.csv",header=0)
		dflistIIGS.append(df)

	for i in range(numPubStatesGP):
		df = read_csv(pathGP+"_"+str(i)+"_losses.csv",header=0)
		dflistGP.append(df)

	besthuberperPubStateOZ = []
	bestl1perPubStateOZ = []
	bestlinfperPubStateOZ = []

	besthuberperPubStateIIGS = []
	bestl1perPubStateIIGS = []
	bestlinfperPubStateIIGS = []

	besthuberperPubStateGP = []
	bestl1perPubStateGP = []
	bestlinfperPubStateGP = []


	for i in range(numPubStatesOZ):
		besthuberperPubStateOZ.append(min(dflistOZ[i]["val_huber_loss"]))
		bestl1perPubStateOZ.append(min(dflistOZ[i]["val_l1_loss"]))
		bestlinfperPubStateOZ.append(min(dflistOZ[i]["val_linf_loss"]))

	for i in range(numPubStatesIIGS):
		besthuberperPubStateIIGS.append(min(dflistIIGS[i]["val_huber_loss"]))
		bestl1perPubStateIIGS.append(min(dflistIIGS[i]["val_l1_loss"]))
		bestlinfperPubStateIIGS.append(min(dflistIIGS[i]["val_linf_loss"]))

	for i in range(numPubStatesGP):
		besthuberperPubStateGP.append(min(dflistGP[i]["val_huber_loss"]))
		bestl1perPubStateGP.append(min(dflistGP[i]["val_l1_loss"]))
		bestlinfperPubStateGP.append(min(dflistGP[i]["val_linf_loss"]))

	meanhuberOZ = np.mean(besthuberperPubStateOZ)
	meanhuberIIGS = np.mean(besthuberperPubStateIIGS)
	meanhuberGP = np.mean(besthuberperPubStateGP)

	meanvec = [meanhuberGP,meanhuberOZ,meanhuberIIGS]
	numPubStates = [numPubStatesGP,numPubStatesOZ,numPubStatesIIGS]
	from scipy.stats import pearsonr
	cor = pearsonr(meanvec,numPubStates)
	print(cor)
	plt.plot(numPubStates,meanvec)
	plt.show()


	###


def plotCrossValAllDomainsThreePlots(domain,save):
	numPubStates = None
	dflist = []
	path = None
	x = None
	if domain=="OZ8C3B":
		path = "experiment_results/publicstate_crossval/OZ8C3B_publicstate"
		numPubStates = 17

	elif domain == "IIGS5T4":
		path = "experiment_results/publicstate_crossval/IIGS5T4_publicstate"
		numPubStates = 9

	elif domain == "GP322221":
		path = "experiment_results/publicstate_crossval/GP322221_publicstate"
		numPubStates = 87

	for i in range(numPubStates):
		df = read_csv(path+"_"+str(i)+"_losses.csv",header=0)
		dflist.append(df)

	besthuberperPubState = []
	bestl1perPubState = []
	bestlinfperPubState = []

	x = np.arange(numPubStates)

	for i in range(numPubStates):
		besthuberperPubState.append(min(dflist[i]["val_huber_loss"]))
		bestl1perPubState.append(min(dflist[i]["val_l1_loss"]))
		bestlinfperPubState.append(min(dflist[i]["val_linf_loss"]))

	###
	plt.rcParams['ytick.labelsize'] = 40
	plt.rcParams['xtick.labelsize'] = 40
	fig, axes = plt.subplots(1, 3, figsize=(20, 5))
	fig.subplots_adjust(wspace=0.05, top=0.99, right=0.95)

	axes[0].bar(x -0.2, besthuberperPubState, width=0.2,align='center',label="Val_Huber",color="blue")

	axes[0].set_ylabel("Huber", fontsize=40)
	axes[0].set_xlabel("")

	# axes[0].yaxis.set_major_formatter(major_formatter)

	axes[0].ticklabel_format(style='sci', axis='y', scilimits=(0, 0))

	axes[1].bar(x, bestl1perPubState, width=0.2, align='center',label="Val_L1",color="orange")
	axes[1].set_ylabel("L-1", fontsize=40)
	axes[1].set_xlabel("Index of public state", fontsize=40)
	# axes[1].yaxis.set_major_formatter(major_formatter)
	axes[1].ticklabel_format(style='sci', axis='y', scilimits=(0, 0))

	axes[2].bar(x +0.2, bestlinfperPubState, width=0.2, align='center',label="Val_Linf",color="green")

	axes[2].set_ylabel("L-"+inf_string, fontsize=40)
	# axes[2].yaxis.set_major_formatter(major_formatter)

	axes[2].ticklabel_format(style='sci', axis='y', scilimits=(0, 0))

	# plt.ylabel("Loss",fontsize=35)
	# plt.xlabel("Index of public state",fontsize=35)

	if save:
		plt.savefig("experiment_results/lossvsexpl_plots/" + domain + "_lossvsexpl.pdf", bbox_inches='tight',
		            pad_inches=0)
	plt.tight_layout()
	plt.show()

def main(args):
	plotCrossValAllDomains3subfigs(args.save)

if __name__ == "__main__":
	main(args)


