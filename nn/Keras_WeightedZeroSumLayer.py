import keras
import keras.backend as K
import numpy as np


class ZeroSumLayer(keras.layers.Layer):

	def __init__(self, output_dim, numPubFeatures, **kwargs):
		super(ZeroSumLayer, self).__init__()
		self.output_dim = output_dim
		self.numPubFeatures = numPubFeatures
		self.name = "ZeroSumLayer"

	def EnforceZeroSum(self, preds, features):
		slicedranges = K.slice(features, [0, self.numPubFeatures], [-1, -1])

		normalizedbyrange = keras.layers.multiply([preds, slicedranges])

		zerosumerror = K.sum(normalizedbyrange)

		return preds - zerosumerror / 2

	def EnforceZeroSumPerPlayer(self, cfvpreds, features):
		## TODO completely wrong. only sum of both players ranges * cfv should sum to 0
		ranges = K.slice(features, [0, self.numPubFeatures], [-1, -1])

		playerprop = int(self.output_dim / 2)

		normalizedbyrange = keras.layers.multiply([cfvpreds, ranges])

		p0weightedsum = K.slice(normalizedbyrange, [0, 0], [-1, playerprop])

		cfvpredsp0 = K.slice(cfvpreds, [0, 0], [-1, playerprop])

		cfvpredsp1 = K.slice(cfvpreds, [0, playerprop], [-1, -1])

		zerosumerrorp0 = K.sum(p0weightedsum)

		p1weightedsum = K.slice(normalizedbyrange, [0, playerprop], [-1, -1])

		zerosumerrorp1 = K.sum(p1weightedsum)

		correctedcfvpredsp0 = cfvpredsp0 - zerosumerrorp0

		correctedcfvpredsp1 = cfvpredsp1 - zerosumerrorp1

		return K.concatenate([correctedcfvpredsp0, correctedcfvpredsp1])

	def call(self, inputs, **kwargs):
		assert isinstance(inputs, list)
		preds, features = inputs
		return ZeroSumLayer.EnforceZeroSum(self, preds, features)

	def compute_output_shape(self, input_shape):
		return (input_shape[0][0], self.output_dim)

	def get_config(self):
		base_config = super(ZeroSumLayer, self).get_config()
		base_config['output_dim'] = self.output_dim
		base_config['numPubFeatures'] = self.numPubFeatures
		base_config['name'] = self.name
		return base_config

## Example test

# mypreds = np.array([-3.9287845456e-18,-4.9336846170800005e-20,-2.52514159909e-17,3.9287845456e-18,4.9336846170800005e-20,2.52514159909e-17])
# myfeatures = np.array([0.0,0.0,0.0,0.193262,0.0650074,0.741731,0.193262,0.0650074,0.741731])
# myfeaturesbatch = np.vstack([myfeatures,myfeatures])
# myNumPubFeatures = 3
# mypredsbatch = np.vstack([mypreds,mypreds])
#
# inputlayer = keras.layers.Input(shape=(myfeatures.shape[0],))
# dense1 = keras.layers.Dense(mypreds.shape[0]) (inputlayer)
# zerosuml = ZeroSumLayer(inputlayer.shape, 3, 6)([dense1, inputlayer])
# model = keras.Model(inputs=inputlayer,outputs=zerosuml)
# model.compile(loss="mse",optimizer=keras.optimizers.Adam())
# model.summary()
# print(np.sum(np.multiply(myfeatures[myNumPubFeatures:],model.predict(np.reshape(myfeatures,newshape=(1,9))))))


#
