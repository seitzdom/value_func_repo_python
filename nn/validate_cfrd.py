#!/usr/bin/python
from nn.build_nn import *
import pandas as pd


def main(domain):

	domain = domain

	actionseq = None
	ps_num = None
	input_shape = None
	output_shape =None
	n_hidden = None
	width_num = None
	seed = None
	nn_input = None
	nn_output = None
	cfrd_in=None
	cfrd_out=None

	if domain=="GP322221":
		print("GP322221")
		actionseq = 3
		ps_num = 27
		input_shape = 2 * actionseq + ps_num
		output_shape = 2 * actionseq
		n_hidden = 2
		width_num = 10
		seed = 1
		nn_input = pd.read_csv('data/data_poker/input_poker.csv')
		nn_output = pd.read_csv('data/data_poker/output_poker.csv')
		cfrd_in = pd.read_csv("heatmap_data/GP322221/cfrd_data/GP322221_cfrd_input.csv")
		cfrd_out = pd.read_csv("heatmap_data/GP322221/cfrd_data/GP322221_cfrd_output.csv")


	elif domain == "IIGS5T4":
		print("IIGS5T4")
		actionseq = 20
		ps_num = 5
		input_shape = 2 * actionseq + ps_num
		output_shape = 2 * actionseq
		n_hidden = 2
		width_num = 10
		seed = 1
		nn_input = pd.read_csv('data/data_iigs5t4/input_gs5_trunk4.csv')
		nn_output = pd.read_csv('data/data_iigs5t4/output_gs5_trunk4.csv')
		cfrd_in = pd.read_csv("heatmap_data/IIGS5T4/cfrd_data/IIGS5T4_cfrd_input.csv")
		cfrd_out = pd.read_csv("heatmap_data/IIGS5T4/cfrd_data/IIGS5T4_cfrd_output.csv")
		batchsize=100

	elif domain == "OZ8C3B":
		print("OZ8C3B")
		actionseq = 35
		ps_num = 7
		input_shape = 2 * actionseq + ps_num
		output_shape = 2 * actionseq
		n_hidden = 2
		width_num = 10
		seed = 1
		nn_input = pd.read_csv("data/data_OZ8CB3/input_osh_c8l1_trunk6_2000.csv")
		nn_output = pd.read_csv("data/data_OZ8CB3/output_osh_c8l1_trunk6_2000.csv")
		cfrd_in = pd.read_csv("heatmap_data/OZ8C3B/input_osh_c8l1_trunk6_cfrd.csv")
		cfrd_out = pd.read_csv("heatmap_data/OZ8C3B/output_osh_c8l1_trunk6_cfrd.csv")

	model = build_fnn(input_shape, output_shape, n_hidden, width_num, "relu", seed)
	model.compile(optimizer=keras.optimizers.Adam(), loss=huber_loss,
	              metrics=[huber_loss, l1_loss, linf_loss, l0_loss])


	CSVLogger=keras.callbacks.CSVLogger("experiment_results/val_cfrd/"+domain+"_cfrdlosses" + ".csv")

	model.fit(x=nn_input, y=nn_output, batch_size=100, epochs=200, verbose=1,callbacks=[CSVLogger],validation_data=[cfrd_in,cfrd_out])

	del model
	K.clear_session()

if __name__ == "__main__":
	for domain in ["OZ8C3B","IIGS5T4"]:

		main(domain)
