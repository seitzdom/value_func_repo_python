from nn.build_nn import *
from nn.loss_functions import *
import keras
import numpy as np
import pandas as pd
from sklearn.preprocessing import OneHotEncoder
import matplotlib.pyplot as plt
from keras.preprocessing.sequence import pad_sequences,TimeseriesGenerator


def create_network_246_depths(ListInputShapes, ListOutputShapes):
    input_t2 = keras.layers.Input(shape=(ListInputShapes[0],))
    input_t4 = keras.layers.Input(shape=(ListInputShapes[1],))
    input_t6 = keras.layers.Input(shape=(ListInputShapes[2],))

    concatlayer = keras.layers.concatenate([input_t2, input_t4, input_t6])

    normalize = keras.layers.BatchNormalization()(concatlayer)

    dense_t2_t4_t6_encoder1 = keras.layers.Dense(
        units=(ListInputShapes[0] + ListInputShapes[1] + ListInputShapes[2]) * 4, activation="relu")(
        normalize)
    dense_t2_t4_t6_encoder2 = keras.layers.Dense(
        units=(ListInputShapes[0] + ListInputShapes[1] + ListInputShapes[2]) * 4, activation="relu")(
        dense_t2_t4_t6_encoder1)

    dense_t2_decoder = keras.layers.Dense(units=ListOutputShapes[0] * 2, activation="relu")(dense_t2_t4_t6_encoder2)
    dense_t4_decoder = keras.layers.Dense(units=ListOutputShapes[1] * 2, activation="relu")(dense_t2_t4_t6_encoder2)
    dense_t6_decoder = keras.layers.Dense(units=ListOutputShapes[2] * 2, activation="relu")(dense_t2_t4_t6_encoder2)

    dense_t2_out = keras.layers.Dense(units=ListOutputShapes[0], activation="linear")(dense_t2_decoder)
    dense_t4_out = keras.layers.Dense(units=ListOutputShapes[1], activation="linear")(dense_t4_decoder)
    dense_t6_out = keras.layers.Dense(units=ListOutputShapes[2], activation="linear")(dense_t6_decoder)

    t2_t4_t6_model = keras.models.Model(inputs=[input_t2, input_t4, input_t6],
                                        outputs=[dense_t2_out, dense_t4_out, dense_t6_out])

    t2_t4_t6_model.compile(keras.optimizers.Adam(), l1_loss, metrics=[huber_loss, linf_loss])

    return t2_t4_t6_model


def train_network_depth_246(listofInputDataframes, listofOutputDataframes,listoflenpublicfeatures,plot=True):
    input_shape_df1 = listofInputDataframes[0].shape[1]
    input_shape_df2 = listofInputDataframes[1].shape[1]
    input_shape_df3 = listofInputDataframes[2].shape[1]

    output_shape_df1 = listofOutputDataframes[0].shape[1]
    output_shape_df2 = listofOutputDataframes[1].shape[1]
    output_shape_df3 = listofOutputDataframes[2].shape[1]

    numPublicFeaturest0 = listoflenpublicfeatures[0]
    numPublicFeaturest1 = listoflenpublicfeatures[1]
    numPublicFeaturest2 = listoflenpublicfeatures[2]

    listInputShapes = [input_shape_df1, input_shape_df2, input_shape_df3]
    listOutputShapes = [output_shape_df1, output_shape_df2, output_shape_df3]

    model = create_network_246_depths(listInputShapes, listOutputShapes)

    onehotdf0 = OneHotEncoder()
    onehotdf1 = OneHotEncoder()
    onehotdf2 = OneHotEncoder()

    onehotdf0.fit(listofInputDataframes[0].iloc[:, :numPublicFeaturest0])
    onehotdf1.fit(listofInputDataframes[1].iloc[:, :numPublicFeaturest1])
    onehotdf2.fit(listofInputDataframes[2].iloc[:, :numPublicFeaturest2])

    onehotdf0.transform(listofInputDataframes[0].iloc[:, :numPublicFeaturest0])
    onehotdf1.transform(listofInputDataframes[1].iloc[:, :numPublicFeaturest1])
    onehotdf2.transform(listofInputDataframes[2].iloc[:, :numPublicFeaturest2])

    training_history = model.fit(x=listofInputDataframes, y=listofOutputDataframes, batch_size=27, epochs=5000,
                                 verbose=1)

    if plot:
        plt.plot(training_history.history['loss'])
        plt.show()

    return training_history


def convert_GS3T2_to_onehot(nninputorig):
    nninput = nninputorig.copy()
    for i in range(nninput.shape[0]):
        if nninput.values[i, 1] == 9.:
            nninput.values[i, 0] = 1
            nninput.values[i, 1] = 0
            nninput.values[i, 2] = 0
        elif nninput.values[i, 1] == 8.:
            nninput.values[i, 0] = 0
            nninput.values[i, 1] = 1
            nninput.values[i, 2] = 0
        elif nninput.values[i, 1] == 10.:
            nninput.values[i, 0] = 0
            nninput.values[i, 1] = 0
            nninput.values[i, 2] = 1
    return nninput.values


def convert_GS3T4_to_onehot(nninputorig):
    nninput = nninputorig.copy()
    for i in range(nninput.shape[0]):
        if nninput.values[i, 1] == 9.:
            nninput.values[i, 0] = 1
            nninput.values[i, 1] = 0
            nninput.values[i, 2] = 0
        elif nninput.values[i, 1] == 8.:
            nninput.values[i, 0] = 0
            nninput.values[i, 1] = 1
            nninput.values[i, 2] = 0
        elif nninput.values[i, 1] == 10.:
            nninput.values[i, 0] = 0
            nninput.values[i, 1] = 0
            nninput.values[i, 2] = 1

        elif nninput.values[i, 4] == 4.:
            nninput.values[i, 3] = 1
            nninput.values[i, 4] = 0
            nninput.values[i, 5] = 0
        elif nninput.values[i, 4] == 5.:
            nninput.values[i, 3] = 0
            nninput.values[i, 4] = 1
            nninput.values[i, 5] = 0
        elif nninput.values[i, 4] == 6.:
            nninput.values[i, 3] = 0
            nninput.values[i, 4] = 0
            nninput.values[i, 5] = 1

    return nninput.values


# nn_input_t2_onehot = convert_GS3T2_to_onehot(nn_input_t2)
# nn_input_t4_onehot = convert_GS3T4_to_onehot(nn_input_t4)
#
# inputshape_t2 = nn_input_t2_onehot.shape[1]
# inputshape_t4 = nn_input_t4_onehot.shape[1]
#
# outputshape_t2 = nn_output_t2.shape[1]
# outputshape_t4 = nn_output_t4.shape[1]
#
# input_t2 = keras.layers.Input(shape=(inputshape_t2,))
# input_t4 = keras.layers.Input(shape=(inputshape_t4,))
#
# concatlayer = keras.layers.concatenate([input_t2, input_t4])
#
# dense_t2_t4_encoder1 = keras.layers.Dense(units=(inputshape_t2 + inputshape_t4) * 4, activation="relu")(concatlayer)
# dense_t2_t4_encoder2 = keras.layers.Dense(units=(inputshape_t2 + inputshape_t4) * 4, activation="relu")(
#     dense_t2_t4_encoder1)
#
# dense_t2_decoder = keras.layers.Dense(units=outputshape_t2 * 2, activation="relu")(dense_t2_t4_encoder2)
# dense_t4_decoder = keras.layers.Dense(units=outputshape_t4 * 2, activation="relu")(dense_t2_t4_encoder2)
#
# dense_t2_out = keras.layers.Dense(units=outputshape_t2, activation="linear")(dense_t2_decoder)
# dense_t4_out = keras.layers.Dense(units=outputshape_t4, activation="linear")(dense_t4_decoder)
#
# t2_t4_model = keras.models.Model(inputs=[input_t2, input_t4], outputs=[dense_t2_out, dense_t4_out])
#
# t2_t4_model.compile(keras.optimizers.Adam(), "mse")
#
# t2_t4_model.fit(x=[nn_input_t2_onehot, nn_input_t4_onehot[:nn_input_t2_onehot.shape[0], :]],
#                 y=[nn_output_t2, nn_output_t4.values[:nn_output_t2.shape[0], :]], batch_size=3, epochs=1000, verbose=1)
#
# mypreds = t2_t4_model.predict([nn_input_t2_onehot[0, :].reshape(1, nn_input_t2_onehot.shape[1]),
#                                nn_input_t4_onehot[0, :].reshape(1, nn_input_t4_onehot.shape[1])])
#
# print(mypreds[0])
# print(nn_output_t2.values[0, :])
# print(mypreds[1])
# print(nn_output_t4.values[0, :])

def convert_targets_to_binary_classes(df):
    dfcopy = df.copy()
    dfcopy = np.array(dfcopy)
    dfcopy[dfcopy>0]=1
    dfcopy[dfcopy <= 0] = 0
    return dfcopy

# t6model = keras.models.Sequential()
# t6model.add(keras.layers.Dense(units=63,activation="linear"))
# t6model.add(keras.layers.Dense(units=63*3,activation="sigmoid"))
# t6model.add(keras.layers.Dense(units=63*3,activation="sigmoid"))
# t6model.add(keras.layers.Dense(units=48,activation="sigmoid"))
#
# t6model.compile(optimizer=keras.optimizers.Adam(),loss=keras.losses.binary_crossentropy)
#
# t6model.fit(x=newinputt6,y=convert_targets_to_binary_classes(nn_output_t6.values),batch_size=10,epochs=100)
#
# predsidx0 = t6model.predict(newinputt6[5,:].reshape(1,63))


## TODO hierarchical MIL network with padding
## TODO output size is the the biggest possible output (lowest trunk) while the network predicts
# padded values for higher infosets

## TODO idea: transform problem into 2 class classification problem: targets <=0 are 0 and targets >0 1

def main():
    numPossibleHandst2 = 3
    numPossibleHandst4 = 6
    numPossibleHandst6 = 24

    numPublicFeaturest2 = 3
    numPublicFeaturest4 = 4
    numPublicFeaturest6 = 9

    nn_input_t2 = pd.read_csv("/home/dominik/Desktop/cpp_seeds/IIGS3T2_random_input.csv")
    nn_output_t2 = pd.read_csv("/home/dominik/Desktop/cpp_seeds/IIGS3T2_random_output.csv")
    nn_input_t4 = pd.read_csv("/home/dominik/Desktop/cpp_seeds/IIGS3T4_random_input.csv")
    nn_output_t4 = pd.read_csv("/home/dominik/Desktop/cpp_seeds/IIGS3T4_random_output.csv")

    nn_input_t6 = pd.read_csv("/home/dominik/Desktop/cpp_seeds/gs4/gs4t6/IIGS4T6_random_input.csv")
    nn_output_t6 = pd.read_csv("/home/dominik/Desktop/cpp_seeds/gs4/gs4t6/IIGS4T6_random_output.csv")

    train_network_depth_246([nn_input_t2, nn_input_t4.iloc[:27,:], nn_input_t6.iloc[:27,:]], [nn_output_t2, nn_output_t4.iloc[:27,:], nn_output_t6.iloc[:27,:]],
                            [numPublicFeaturest2, numPublicFeaturest4, numPublicFeaturest6])


##TODO create network which predicts just one (aug) infoset and takes encoding of PS + rng(I) + convolved opp ranges as input
##TODO maybe also try to predict flattened softmax vector of all possible actions in the whole game (with illegal ones masked)

def padVector(vec,maxlen,padding_value=-100):
    if len(vec) == maxlen:
        return vec
    else:
        return np.reshape(np.concatenate((vec,np.array([padding_value for i in range(maxlen-len(vec))]))),newshape=(1,maxlen))

def padMatrix(mat,maxlen,padding_value=-100):
    if mat.shape[1] == maxlen:
        return mat
    else:

        for i in range(mat.shape[0]):
            paddedvec = padVector(np.array(mat)[i,:],maxlen,padding_value)
            if i == 0:
                paddedmat = paddedvec
            else:
                paddedmat = np.concatenate((paddedmat,paddedvec),axis=0)
        return paddedmat

def getLSTMinputoutput(data,targets,numTrunkdepths):
    return keras.preprocessing.sequence.TimeseriesGenerator(data,targets,numTrunkdepths)

def computeMaxAmountofActions():
    pass

def maskIllegalActions():
    pass

def getCFVforAugInfoset():
    pass

def lstm_vf_alldepths(x,y):

    masking_value = -100

    dim =0

    keras.layers.Input()
    keras.layers.Masking(mask_value=masking_value,input_shape=(None,dim))

def pad_transform_data(datalist):
    maxseqlen = 0
    currentmaxlen = 0
    for i in range(len(datalist)):
        currentmaxlen = datalist[i].shape[1]
        if currentmaxlen > maxseqlen:
            maxseqlen = currentmaxlen
    return [padMatrix(mat,maxseqlen,-100) for mat in datalist]

##TODO get indices of parent infosets for each trunk depth and associate the infoset trunkdepths. or rather child action seq


## TODO network that creates embedding of public obs + private obs +ranges + player and outputs one prediction
## TODO for the CFV of the corresponding (aug) infoset

def lstmEncodePubObs(numMaxPubFeatures,numMaxPubStates):
    pass

def lstmEncodePrivateObs(numMaxPrivateFeatures,numMaxActions):
    pass

def convRangesP0P1():
    pass

def denseRangesP0P1():
    pass

def boolGetPlayer():
    pass

def embedPS(pubstatencoding):
    pass

def embedActSeq(actseqencoding):
    pass

def getPlayer(vectorplayers):
    pass

def examplestuff():
    ps_encoding = np.array([0,1,0]) # second possible public obs
    actseq_encoding = np.array([0, 0, 1]) # third action
    rangesp0p1 = np.array([0.33 for i in range(6)])
    targets = np.array([0 for i in range(6)])
    target0 = targets[0]
    players = np.array([0,1])

    ps_embedded = embedPS(ps_encoding)
    actseq_embedded = embedActSeq(actseq_encoding)
    player = getPlayer(players)

def lstmembeddingtest():
    numtimesteps = 2
    numfeatures = 3
    nn_input_t2 = pd.read_csv("/home/dominik/Desktop/cpp_seeds/IIGS3T2_random_input.csv")
    nn_output_t2 = pd.read_csv("/home/dominik/Desktop/cpp_seeds/IIGS3T2_random_output.csv")
    nn_input_t4 = pd.read_csv("/home/dominik/Desktop/cpp_seeds/IIGS3T4_random_input.csv")
    nn_output_t4 = pd.read_csv("/home/dominik/Desktop/cpp_seeds/IIGS3T4_random_output.csv")
    onehotinputtgs3t2 = convert_GS3T2_to_onehot(nn_input_t2)
    onehotinputtgs3t4 = convert_GS3T4_to_onehot(nn_input_t4)
    # longpub = np.array([[0,1,0],[0,1,0]])
    # longpriv = np.array([[0, 0, 1], [0, 1, 0]])
    # shortpub = np.array([0, 1, 0])
    # shortpriv = np.array([0, 0, 1])

    pubencoder = keras.layers.LSTM(units=6,return_sequences=True,batch_input_shape=(None,numtimesteps,numfeatures))
    privencoder = keras.layers.LSTM(units=6,return_sequences=True,batch_input_shape=(None,numtimesteps,numfeatures))
    convencode = keras.layers.Conv1D(filters=5,kernel_size=(6),input_shape=())




if __name__ == "__main__":

   main()