from nn.build_nn import *
from nn.custom_encoding import *
from nn.domain_constants import *
from keras.optimizers import Adam
import numpy as np
import matplotlib.pyplot as plt
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("--domain", type=str)
parser.add_argument("--networktype",type=str)
parser.add_argument("--encode",default=True,action="store_false")
#Now omitting the switch sets args.encode to True, using --encode (with no further argument) sets args.encode to False
#https://stackoverflow.com/questions/44561722/why-in-argparse-a-true-is-always-true
parser.add_argument("--numSamples",default=3,type=int)
args = parser.parse_args()

numSamplesPerPlayer = args.numSamples
numRandomizations = 10

def matrix_rowwise_randomization(nn_input):
    #TODO just ranges without public features
    from numpy.random import shuffle
    nn_input = np.array(nn_input)
    for i in range(nn_input.shape[0]):
        shuffle(nn_input[i,:])
    return nn_input

def create_Ntimes_randomized_trainingdataset(nn_input,nn_output,numPublicFeatures,numRandomizations):

    ## TODO what we want is to randomly show the ranges to the network in an arbitrary order
    nn_input = np.array(nn_input)

    publicfeatures = nn_input[:,:numPublicFeatures]
    justranges = nn_input[:, numPublicFeatures:]
    randomized_nninput = np.array(nn_input)

    for k in range(numRandomizations):
        kth_randomization = np.concatenate([publicfeatures,matrix_rowwise_randomization(justranges)],axis=1)
        randomized_nninput = np.concatenate([randomized_nninput,kth_randomization],axis=0)

    return randomized_nninput,np.tile(nn_output,(numRandomizations+1,1))

def train_baseline_network(nn_input, nn_output, plot=True):

    model = build_fnn(nn_input.shape[1],nn_output.shape[1],4,100)

    model.compile(optimizer=Adam(), loss='mse')

    training_history = model.fit(x=nn_input, y=nn_output, batch_size=27, epochs=100,
                                 verbose=1)

    if plot:
        plt.plot(training_history.history['loss'])
        plt.show()

    return training_history

def train_randomized_positionnetwork(nn_input, nn_output, numPublicFeatures,numRandomizations, plot=True):

    model = build_fnn(nn_input.shape[1],nn_output.shape[1],4,100)

    model.compile(optimizer=Adam(), loss='mse')

    randomizedtraininginput,randomizedtrainingoutput = create_Ntimes_randomized_trainingdataset(nn_input,nn_output,numPublicFeatures,numRandomizations)

    assert np.any(np.isnan(randomizedtraininginput)) == False
    assert (randomizedtraininginput < 100).all()
    training_history = model.fit(x=randomizedtraininginput, y=randomizedtrainingoutput, batch_size=27, epochs=100,
                                 verbose=1)

    if plot:
        plt.plot(training_history.history['loss'])
        plt.show()

    return training_history

def create_minmeanmax_rangesinput(nn_input):
    nn_input = np.array(nn_input)
    new_nninput = None
    for i in range(nn_input.shape[0]):
        minrng = np.min(nn_input[i,:])
        meanrng = np.mean(nn_input[i,:])
        maxrng = np.max(nn_input[i,:])
        if i == 0:
            new_nninput = np.array([minrng,meanrng,maxrng]).reshape(1,3)
        else:
            new_nninput = np.concatenate([new_nninput,np.array([minrng,meanrng,maxrng]).reshape(1,3)],axis=0)
    return new_nninput

def sample_N_fromRanges(playerranges,N):
    from numpy.random import choice
    playerranges = np.array(playerranges)
    sampledrangesmatrix = None
    for i in range(playerranges.shape[0]):
        if i == 0:
            sampledrangesmatrix = choice(playerranges[i,:],N,replace=False).reshape(1,N)
        else:
            sampledrangesmatrix = np.concatenate([sampledrangesmatrix,choice(playerranges[i,:],N,replace=False).reshape(1,N)],axis=0)
    return sampledrangesmatrix



def train_minmeanmax_poolingranges_network(nn_input,nn_output,numPublicFeatures,plot=True):

    numPoolings = 3
    model = build_fnn(numPublicFeatures+numPoolings,nn_output.shape[1],4,100)
    model.compile(optimizer=Adam(), loss='mse')
    publicfeatures = nn_input[:, :numPublicFeatures]
    justranges = nn_input[:, numPublicFeatures:]

    ranges_minmeanmax = create_minmeanmax_rangesinput(justranges)

    nn_input_minmeanmax = np.concatenate([publicfeatures,ranges_minmeanmax],axis=1)

    training_history = model.fit(x=nn_input_minmeanmax, y=nn_output, batch_size=27, epochs=1000,
                                 verbose=1)

    if plot:
        plt.plot(training_history.history['loss'])
        plt.show()

    return training_history

def train_playerwise_minmeanmax_poolingranges_network(nn_input,nn_output,numPublicFeatures,numPossibleHandsPerPlayer,plot=True,epochs=100):
    numPoolingsPerPlayer = 3
    model = build_fnn(numPublicFeatures+(numPoolingsPerPlayer*2),nn_output.shape[1],4,100)

    model.compile(optimizer=Adam(), loss='mse')

    publicfeatures = nn_input[:, :numPublicFeatures]
    justranges = nn_input[:, numPublicFeatures:]

    rangesp0 = justranges[:,:numPossibleHandsPerPlayer]
    rangesp1 = justranges[:, numPossibleHandsPerPlayer:]

    ranges_minmeanmaxp0 = create_minmeanmax_rangesinput(rangesp0)
    ranges_minmeanmaxp1 = create_minmeanmax_rangesinput(rangesp1)

    print(ranges_minmeanmaxp0.shape)
    print(ranges_minmeanmaxp1.shape)

    nn_input_minmeanmax = np.concatenate([publicfeatures,ranges_minmeanmaxp0,ranges_minmeanmaxp1],axis=1)

    training_history = model.fit(x=nn_input_minmeanmax, y=nn_output, batch_size=27, epochs=epochs,
                                 verbose=1)

    if plot:
        plt.plot(training_history.history['loss'])
        plt.show()

    return training_history


def fixed_size_sampling_nn(nn_input, nn_output, numPublicFeatures,numPossibleHandsPerPlayer,numSampledRangesPerPlayer, plot=True, epochs=100):
        model = build_fnn(numPublicFeatures + (numSampledRangesPerPlayer * 2), nn_output.shape[1], 4, 100)

        model.compile(optimizer=Adam(), loss='mse')

        publicfeatures = nn_input[:, :numPublicFeatures]
        justranges = nn_input[:, numPublicFeatures:]

        rangesp0 = justranges[:, :numPossibleHandsPerPlayer]
        rangesp1 = justranges[:, numPossibleHandsPerPlayer:]

        sampledranges_p0 = sample_N_fromRanges(rangesp0,numSampledRangesPerPlayer)
        sampledranges_p1 = sample_N_fromRanges(rangesp1,numSampledRangesPerPlayer)

        print(sampledranges_p0.shape)
        print(sampledranges_p1.shape)

        nn_input_sampled = np.concatenate([publicfeatures, sampledranges_p0, sampledranges_p1], axis=1)

        training_history = model.fit(x=nn_input_sampled, y=nn_output, batch_size=27, epochs=epochs,verbose=1)

        if plot:
            plt.plot(training_history.history['loss'])
            plt.show()

        return training_history


def fixed_size_sampling_pooling_nn(nn_input, nn_output, numPublicFeatures, numPossibleHandsPerPlayer, numSampledRangesPerPlayer,
                           plot=True, epochs=100):
    model = build_fnn(numPublicFeatures + numSamplesPerPlayer*2, nn_output.shape[1], 4, 100)

    model.compile(optimizer=Adam(), loss='mse')

    publicfeatures = nn_input[:, :numPublicFeatures]
    justranges = nn_input[:, numPublicFeatures:]

    rangesp0 = justranges[:, :numPossibleHandsPerPlayer]
    rangesp1 = justranges[:, numPossibleHandsPerPlayer:]

    sampledranges_p0 = sample_N_fromRanges(rangesp0, numSampledRangesPerPlayer)
    sampledranges_p1 = sample_N_fromRanges(rangesp1, numSampledRangesPerPlayer)

    sampledpooledranges_p0 = create_minmeanmax_rangesinput(sampledranges_p0)
    sampledpooledranges_p1 = create_minmeanmax_rangesinput(sampledranges_p1)

    print(sampledranges_p0.shape)
    print(sampledranges_p1.shape)

    nn_input_sampled = np.concatenate([publicfeatures, sampledpooledranges_p0, sampledpooledranges_p1], axis=1)

    training_history = model.fit(x=nn_input_sampled, y=nn_output, batch_size=27, epochs=epochs, verbose=1)

    if plot:
        plt.plot(training_history.history['loss'])
        plt.show()

    return training_history

##TODO add padding network for different depths. non constant sampling size

def main(args):

    nn_input, nn_output = load_data_for_domain(args.domain)

    if args.encode:
        print("encoding domain")
        nn_input = onehot_enc_domain(nn_input,args.domain)

    numPossibleHands,numPublicFeatures = getDomainConstantsFor(args.domain)

    if args.networktype == "randomposition":
        train_randomized_positionnetwork(nn_input,nn_output,numPublicFeatures,numRandomizations)

    elif args.networktype == "pooling":
        train_minmeanmax_poolingranges_network(nn_input,nn_output,numPublicFeatures)

    elif args.networktype == "playerpooling":
        train_playerwise_minmeanmax_poolingranges_network(nn_input,nn_output,numPublicFeatures,numPossibleHands)

    elif args.networktype == "sampling":
        fixed_size_sampling_nn(nn_input, nn_output, numPublicFeatures, numPossibleHands,numSamplesPerPlayer)

    elif args.networktype == "samplingpooling":
        fixed_size_sampling_pooling_nn(nn_input, nn_output, numPublicFeatures, numPossibleHands,numSamplesPerPlayer)

    elif args.networktype == "baseline":
        train_baseline_network(nn_input, nn_output)
    else:
        NotImplementedError



if __name__ == "__main__":

   main(args)