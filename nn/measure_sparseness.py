from process_data.SeedsToNNInput import *
import pandas as pd
from nn.build_nn import *


def sparsenessratio(x, r1column, r2column):
	rationonzeror1 = len(np.nonzero(x.iloc[:, r1column])[0]) / x.shape[0]
	rationonzeror2 = len(np.nonzero(x.iloc[:, r2column])[0]) / x.shape[0]
	return (rationonzeror1 + rationonzeror2) / 2


# if __name__ == "__main__":
# 	r1column = 10
# 	r2column = 11
# 	sparsenesslist = []
# 	filepath = "/home/dominik/Desktop/normalpokerseeds/"
# 	filelist = readfile(filepath)
# 	for file in filelist:
# 		seed = pd.read_csv(file, header=None)
# 		sparsenesslist.append(sparsenessratio(seed,r1column,r2column))
# 	meansparseness = np.mean(sparsenesslist)
# 	print(meansparseness)

if __name__ == "__main__":
	root = "/home/dominik/Desktop/fullymixed_poker_seeds_0_3/"
	r1column = 33
	r2column = 34
	sparsenesslist = []
	filelist = ReadSeedsFromPath(root)
	for file in filelist:
		seed = pd.read_csv(file, header=None)
		sparsenesslist.append(sparsenessratio(seed, r1column, r2column))
	meansparseness = np.mean(sparsenesslist)
	print(meansparseness)

	nn_input, nn_output = SeedPathToNNInput(filelist, 3, 27)
	pd.DataFrame(nn_input).to_csv(root + 'data/input_poker.csv', index=False)
	pd.DataFrame(nn_output).to_csv(root + 'data/output_poker.csv', index=False)
