#!/usr/bin/python

import pandas as pd
from keras.models import load_model
import glob
import csv
from nn.build_nn import *
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("--networkdir", type=str)
parser.add_argument("--domain", type=str)
parser.add_argument("--networkname", type=str)
parser.add_argument("--numnetworks", type=int)
parser.add_argument("--expltxtpath", type=str)
args = parser.parse_args()

def np_linf_loss(y_true, y_pred):
    y_true= np.array(y_true)
    y_pred= np.array(y_pred)
    acc_loss=0
    for i in range(y_true.shape[0]):
        acc_loss+= np.max(np.abs(y_true[i,:]-y_pred[i,:]))
    return acc_loss/y_true.shape[0]

def np_l1_loss(y_true, y_pred):
    y_true= np.array(y_true)
    y_pred= np.array(y_pred)
    acc_loss=0
    for i in range(y_true.shape[0]):
        acc_loss+= np.mean(np.abs(y_true[i,:]-y_pred[i,:]),axis=0)
    return acc_loss/y_true.shape[0]

def np_huber_loss(y_true, y_pred):
    y_true= np.array(y_true)
    y_pred= np.array(y_pred)
    acc_loss=0
    clip_delta=1
    for i in range(y_true.shape[0]):
        abserrorvec = y_true[i,:]-y_pred[i,:]
        hubererrorvec = []
        for error in abserrorvec:
            if (np.abs(error) < clip_delta):
                hubererrorvec.append(0.5 * np.square(error))
            else:
                hubererrorvec.append(clip_delta * (np.abs(error) - 0.5 * clip_delta))
        acc_loss += np.mean(np.array(hubererrorvec))
    return acc_loss/y_true.shape[0]

def readnetworks(file_directory):
	filelst = [f for f in glob.glob(file_directory + "*.h5", recursive=True)]
	filelst.sort()
	return filelst

datapath = "data"

def validate_networklist(networkfolder,domain,networkname,numnetworks):

	if domain == "GP322221":
		nn_input = pd.read_csv(datapath + '/data_poker/input_poker.csv')
		nn_output = pd.read_csv(datapath + '/data_poker/output_poker.csv')

	elif domain== "IIGS5T4":

		nn_input = pd.read_csv(datapath + '/data_iigs5t4/input_gs5_trunk4.csv')
		nn_output = pd.read_csv(datapath + '/data_iigs5t4/output_gs5_trunk4.csv')

	elif domain=="OZ8CB3":

		nn_input = pd.read_csv(datapath + '/data_OZ8CB3/input_osh_c8l1_trunk6_2000.csv')
		nn_output = pd.read_csv(datapath + '/data_OZ8CB3/output_osh_c8l1_trunk6_2000.csv')

	# networklist=readnetworks(networkfolder)

	explvec = np.array(pd.read_csv(networkfolder+"expl.txt", header=None))

	nn_input_val = nn_input.iloc[-1000:, :]
	nn_output_val = nn_output.iloc[-1000:, :]

	with open(networkfolder +domain+"_LossesExpl.csv", "w") as file:
		writer = csv.writer(file, delimiter=",")
		writer.writerow(["#", "huber", "l1", "linf","expl"])

	for i in range(numnetworks):
		model = load_model(networkfolder+networkname+"_"+str(i+1)+".h5",custom_objects={"huber_loss":huber_loss,"l1_loss":l1_loss,"linf_loss":linf_loss,"l0_loss":l0_loss})
		preds = model.predict(x=nn_input_val,batch_size=1000)
		huber = np.round(np_huber_loss(nn_output_val,preds),decimals=4)
		l1 = np.round(np_l1_loss(nn_output_val,preds),decimals=4)
		l_inf = np.round(np_linf_loss(nn_output_val,preds),decimals=4)
		expl = explvec[i][0]

		with open(networkfolder +domain+"_LossesExpl.csv", "a") as file:
			writer = csv.writer(file, delimiter=",")
			writer.writerow([i, huber, l1, l_inf,expl])

def main(args):
	validate_networklist(args.networkdir,args.domain,args.networkname,args.numnetworks)


if __name__ == "__main__":
	main(args)