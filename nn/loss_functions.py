import keras
import keras.backend as K
import numpy as np
import tensorflow as tf
from keras.layers import multiply
from nn.Keras_WeightedZeroSumLayer import *


def l2_sign_loss(y_true, y_pred):
    error = y_true - y_pred
    squared_loss = 0.5 * keras.backend.square(error)
    sign_equal = K.cast(tf.where(K.sign(y_true) == K.sign(y_pred), 1, 0), tf.float32)
    ratiosigncorrect = K.sum(sign_equal, axis=-1) / tf.size(y_true)
    return squared_loss / ratiosigncorrect


# https://www.quora.com/How-can-I-penalize-a-regression-loss-function-to-account-for-correctness-on-the-sign-of-the-prediction

def huber_loss(y_true, y_pred, clip_delta=1.0):
    error = y_true - y_pred
    cond = K.abs(error) < clip_delta
    squared_loss = 0.5 * K.square(error)
    linear_loss = clip_delta * (K.abs(error) - 0.5 * clip_delta)
    return tf.where(cond, squared_loss, linear_loss)


def l0_loss(y_true, y_pred):
    is_pos_true = K.cast(y_true > 0, tf.float32)
    num_pos_true = K.sum(is_pos_true, axis=-1)
    is_pos_pred = K.cast(y_pred > 0, tf.float32)
    num_pos_pred = K.sum(is_pos_pred, axis=-1)
    return K.abs(num_pos_true - num_pos_pred)

def exact_zero_loss(y_true, y_pred):
    exact_zero_true = K.cast(K.equal(y_true,0),tf.float32)
    num_exact_zero_true = K.sum(exact_zero_true, -1)

    exact_zero_pred = K.cast(K.equal(y_pred, 0), tf.float32)
    num_exact_zero_pred = K.sum(exact_zero_pred, -1)


    return K.abs(num_exact_zero_true - num_exact_zero_pred)


def l0_ratio_loss(y_true, y_pred):
    is_pos_true = K.cast(y_true > 0, tf.float32)
    num_pos_true = K.sum(is_pos_true, axis=-1)
    is_pos_pred = K.cast(y_pred > 0, tf.float32)
    num_pos_pred = K.sum(is_pos_pred, axis=-1)
    ratio_0_true = num_pos_true / K.cast(K.shape(y_true)[1], tf.float32)
    ratio_0_pred = num_pos_pred / K.cast(K.shape(y_pred)[1], tf.float32)
    return ratio_0_true - ratio_0_pred


def l1_loss(y_true, y_pred):
    error = K.abs(y_true - y_pred)
    return K.mean(error, axis=-1)


def linf_loss(y_true, y_pred):
    return K.max(K.abs(y_true - y_pred), axis=-1)


####


def AbsSumLoss(y_true, y_pred):
    error = K.abs(y_true - y_pred)
    return K.sum(error, axis=-1)


def Vilo_Loss(y_true, y_pred):
    ## l(x,y) = \sum_{i,j \in {1..dim} ((1+x_i)/(1+x_j) - (1+y_i)/(1+y_j))^2
    pass


def getCertainLoss(value):
    def loss(y_true, y_pred):
        error = linf_loss(y_true, y_pred)
        return K.abs(value - error)

    return loss


def KLD_CFV(y_true, y_pred):
    y_true = keras.activations.relu(y_true)
    y_pred = keras.activations.relu(y_pred)
    y_true_mean = K.mean(y_true)
    y_pred_mean = K.mean(y_pred)
    y_true_std = K.std(y_true)
    y_pred_std = K.std(y_pred)
    y_true = (y_true - y_true_mean) / y_true_std
    y_pred = (y_pred - y_pred_mean) / y_pred_std
    y_true = keras.activations.softmax(y_true)
    y_pred = keras.activations.softmax(y_pred)
    return K.sum(y_true * K.log(y_true / y_pred), axis=-1)


def sum_loss(y_true, y_pred):
    return huber_loss(y_true, y_pred, clip_delta=1.0) + linf_loss(y_true, y_pred)


def square_loss(y_true, y_pred):
    return huber_loss(y_true, y_pred, clip_delta=1.0) + linf_loss(y_true, y_pred) ** 2


def reach_weighted_linf(PubStateRanges, numPubFeatures):
    def custom_linf_loss(y_true, y_pred):
        abs_error = np.abs(y_true - y_pred)

        slicedranges = K.slice(PubStateRanges, [0, numPubFeatures], [-1, -1])

        weighted_linf = multiply([slicedranges, abs_error])

        return K.max(weighted_linf)

    return custom_linf_loss


def reach_weighted_l1(PubStateRanges, numPubFeatures):
    def custom_l1_loss(y_true, y_pred):
        abs_error = K.abs(y_true - y_pred)

        slicedranges = K.slice(PubStateRanges, [0, numPubFeatures], [-1, -1])

        weighted_l1 = multiply([slicedranges, abs_error])

        return K.mean(weighted_l1)

    return custom_l1_loss


def reach_weighted_huber(PubStateRanges, numPubFeatures):
    def custom_huber_loss(y_true, y_pred, clip_delta=1.0):
        error = y_true - y_pred
        cond = error < clip_delta
        squared_loss = 0.5 * K.square(error)
        linear_loss = clip_delta * (K.abs(error) - 0.5 * clip_delta)
        huber = tf.where(cond, squared_loss, linear_loss)
        slicedranges = K.slice(PubStateRanges, [0, numPubFeatures], [-1, -1])
        return multiply([slicedranges, huber])

    return custom_huber_loss


def ZeroSumLoss(PubStateRanges, numPubFeatures):
    def zerosum_loss(y_true, y_pred):
        slicedranges = K.slice(PubStateRanges, [0, numPubFeatures], [-1, -1])

        normalizedbyrange_true = keras.layers.multiply([y_true, slicedranges])

        normalizedbyrange_pred = keras.layers.multiply([y_pred, slicedranges])

        error = normalizedbyrange_true - normalizedbyrange_pred

        squared_zero_sum_loss = 0.5 * keras.backend.square(error)

        zerosumerror_true = K.sum(normalizedbyrange_true)

        zerosumerror_pred = K.sum(normalizedbyrange_pred)

        totalzerosumerror = K.abs(zerosumerror_true - zerosumerror_pred)

        return totalzerosumerror

    return zerosum_loss


def ZeroSumSoftmax(PubStateRanges, numPubFeatures):
    def zerosum_loss(y_true, y_pred):
        slicedranges = K.slice(PubStateRanges, [0, numPubFeatures], [-1, -1])

        normalizedbyrange_true = keras.activations.softmax(keras.layers.multiply([y_true, slicedranges]))

        normalizedbyrange_pred = keras.activations.softmax(keras.layers.multiply([y_pred, slicedranges]))

        cross_entropy_error = K.categorical_crossentropy(normalizedbyrange_true, normalizedbyrange_pred)

        error = normalizedbyrange_true - normalizedbyrange_pred

        squared_zero_sum_loss = 0.5 * keras.backend.square(error)

        zerosumerror_true = K.sum(normalizedbyrange_true)

        zerosumerror_pred = K.sum(normalizedbyrange_pred)

        totalzerosumerror = K.abs(zerosumerror_true - zerosumerror_pred)

        return totalzerosumerror

    return zerosum_loss


def GameValueLoss(PubStateRanges, numPubFeatures):
    def gamevalue_loss(y_true, y_pred):
        ##TODO compute game value PER PLAYER of public state for ytrue and ypred. compute loss from difference

        slicedranges = K.slice(PubStateRanges, [0, numPubFeatures], [-1, -1])

        normalizedbyrange_true = keras.layers.multiply([y_true, slicedranges])

        normalizedbyrange_pred = keras.layers.multiply([y_pred, slicedranges])

        # playerprop = K.shape(y_true).eval(session=K.get_session())[1]
        playerprop = 2

        p0weightedsum_true = K.slice(normalizedbyrange_true, [0, 0], [-1, playerprop])

        p1weightedsum_true = K.slice(normalizedbyrange_true, [0, playerprop], [-1, -1])

        p0weightedsum_pred = K.slice(normalizedbyrange_pred, [0, 0], [-1, playerprop])

        p1weightedsum_pred = K.slice(normalizedbyrange_pred, [0, playerprop], [-1, -1])

        gamevalue_error_p0 = K.abs(K.sum(p0weightedsum_true) - K.sum(p0weightedsum_pred))

        gamevalue_error_p1 = K.abs(K.sum(p1weightedsum_true) - K.sum(p1weightedsum_pred))

        return gamevalue_error_p0 + gamevalue_error_p1

    return gamevalue_loss


def HuberZeroSumLoss(PubStateRanges, numPubFeatures):
    def huber_zerosum_loss(y_true, y_pred, clip_delta=1.0):
        error = y_true - y_pred
        cond = keras.backend.abs(error) < clip_delta
        squared_loss = 0.5 * keras.backend.square(error)
        linear_loss = clip_delta * (keras.backend.abs(error) - 0.5 * clip_delta)
        huber = tf.where(cond, squared_loss, linear_loss)

        slicedranges = K.slice(PubStateRanges, [0, numPubFeatures], [-1, -1])

        normalizedbyrange_true = keras.layers.multiply([y_true, slicedranges])

        normalizedbyrange_pred = keras.layers.multiply([y_pred, slicedranges])

        error = normalizedbyrange_true - normalizedbyrange_pred

        squared_zero_sum_loss = 0.5 * keras.backend.square(error)

        zerosumerror_true = K.sum(normalizedbyrange_true)

        zerosumerror_pred = K.sum(normalizedbyrange_pred)

        totalzerosumerror = K.abs(zerosumerror_true - zerosumerror_pred)

        return huber + totalzerosumerror

    return huber_zerosum_loss


def weighted_linf_loss(PubStateRanges, numPubFeatures):
    def custom_linf_loss(y_true, y_pred):
        linf_error = np.max(np.abs(y_true - y_pred))

        slicedranges = K.slice(PubStateRanges, [0, numPubFeatures], [-1, -1])

        normalizedbyrange_true = keras.layers.multiply([y_true, slicedranges])

        # PlayerFraction = K.shape(normalizedbyrange_true).eval(session=K.get_session())[1]/2

        PlayerFraction = 3

        public_state_value_p0 = K.sum(K.slice(normalizedbyrange_true, [0, 0], [-1, PlayerFraction]))

        return linf_error * public_state_value_p0

    return custom_linf_loss


def reach_weighted_linf_loss(PubStateRanges, numPubFeatures):
    def custom_linf_loss(y_true, y_pred):
        error_vec = np.abs(y_true - y_pred)

        slicedranges = K.slice(PubStateRanges, [0, numPubFeatures], [-1, -1])

        weighted_abs_error = np.multiply(slicedranges, error_vec)

        return np.max(weighted_abs_error)

    return custom_linf_loss


def proportional_asymmetric_exact_zero_loss(beta):
    ##penalize errors on samples which are exactly 0
    def custom_l1_loss(y_true, y_pred):
        abs_error = K.abs(y_true - y_pred)
        amplified_truezero_error = K.switch(K.equal(y_true, 0), beta * abs_error, (1-beta) * abs_error)
        return K.mean(amplified_truezero_error,-1)

    return custom_l1_loss

def weighted_asymmetric_exact_zero_loss(beta):
    ##penalize errors on samples which are exactly 0
    def custom_l1_loss(y_true, y_pred):
        abs_error = K.abs(y_true - y_pred)
        amplified_truezero_error = K.switch(K.equal(y_true, 0), beta * abs_error,abs_error)
        return K.mean(amplified_truezero_error,-1)

    return custom_l1_loss

def sign_loss(y_true,y_pred):
    truesign = K.sign(y_true)
    predsign = K.sign(y_pred)
    abssign = K.abs(truesign-predsign)
    return K.mean(abssign,-1)


def heaviside_stepsign_loss(y_true,y_pred):
    truesign = K.sign(y_true)
    predsign = K.sign(y_pred)
    abssign = K.abs(truesign-predsign)





