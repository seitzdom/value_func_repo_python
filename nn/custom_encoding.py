import numpy as np

def convert_GS_N_T2_to_onehot(nninputorig):
    nninput = nninputorig.copy()
    for i in range(nninput.shape[0]):
        if nninput.values[i, 1] == 9.:
            nninput.values[i, 0] = 1
            nninput.values[i, 1] = 0
            nninput.values[i, 2] = 0
        elif nninput.values[i, 1] == 8.:
            nninput.values[i, 0] = 0
            nninput.values[i, 1] = 1
            nninput.values[i, 2] = 0
        elif nninput.values[i, 1] == 10.:
            nninput.values[i, 0] = 0
            nninput.values[i, 1] = 0
            nninput.values[i, 2] = 1
    return nninput.values


def convert_GS_N_T4_to_onehot(nninputorig):
    nninput = nninputorig.copy()

    for i in range(nninput.shape[0]):
        if nninput.values[i, 1] == 9.:
            nninput.values[i, 0] = 1
            nninput.values[i, 1] = 0
            nninput.values[i, 2] = 0
        elif nninput.values[i, 1] == 8.:
            nninput.values[i, 0] = 0
            nninput.values[i, 1] = 1
            nninput.values[i, 2] = 0
        elif nninput.values[i, 1] == 10.:
            nninput.values[i, 0] = 0
            nninput.values[i, 1] = 0
            nninput.values[i, 2] = 1
    for i in range(nninput.shape[0]):
        if nninput.values[i, 4] == 4.:
            nninput.values[i, 3] = 1
            nninput.values[i, 4] = 0
            nninput.values[i, 5] = 0
        elif nninput.values[i, 4] == 5.:
            nninput.values[i, 3] = 0
            nninput.values[i, 4] = 1
            nninput.values[i, 5] = 0
        elif nninput.values[i, 4] == 6.:
            nninput.values[i, 3] = 0
            nninput.values[i, 4] = 0
            nninput.values[i, 5] = 1

    return nninput.values


def convert_GS_N_T6_to_onehot(nninputorig):
    nninput = nninputorig.copy()

    # round 1
    for i in range(nninput.shape[0]):
        if nninput.values[i, 1] == 12.:
            nninput.values[i, 0] = 1
            nninput.values[i, 1] = 0
            nninput.values[i, 2] = 0
        elif nninput.values[i, 1] == 13.:
            nninput.values[i, 0] = 0
            nninput.values[i, 1] = 1
            nninput.values[i, 2] = 0
        elif nninput.values[i, 1] == 14.:
            nninput.values[i, 0] = 0
            nninput.values[i, 1] = 0
            nninput.values[i, 2] = 1

    # round 2

    for i in range(nninput.shape[0]):
        if nninput.values[i, 4] == 8.:
            nninput.values[i, 3] = 1
            nninput.values[i, 4] = 0
            nninput.values[i, 5] = 0
        elif nninput.values[i, 4] == 9.:
            nninput.values[i, 3] = 0
            nninput.values[i, 4] = 1
            nninput.values[i, 5] = 0
        elif nninput.values[i, 4] == 10.:
            nninput.values[i, 3] = 0
            nninput.values[i, 4] = 0
            nninput.values[i, 5] = 1
    # round 3
    for i in range(nninput.shape[0]):
        if nninput.values[i, 7] == 4.:
            nninput.values[i, 6] = 1
            nninput.values[i, 7] = 0
            nninput.values[i, 8] = 0
        elif nninput.values[i, 7] == 5.:
            nninput.values[i, 6] = 0
            nninput.values[i, 7] = 1
            nninput.values[i, 8] = 0
        elif nninput.values[i, 7] == 6.:
            nninput.values[i, 6] = 0
            nninput.values[i, 7] = 0
            nninput.values[i, 8] = 1

    return nninput.values


def convert_targets_to_binary_classes(df):
    dfcopy = df.copy()
    dfcopy = np.array(dfcopy)
    dfcopy[dfcopy > 0] = 1
    dfcopy[dfcopy <= 0] = 0
    return dfcopy

def onehot_enc_domain(nn_input,domain):
    if domain == "gs3t2" or domain == "gs4t2":
        return convert_GS_N_T2_to_onehot(nn_input)
    elif domain == "gs3t4" or domain == "gs4t4":
        return convert_GS_N_T4_to_onehot(nn_input)
    elif domain == "gs3t6" or domain == "gs4t6":
        return convert_GS_N_T6_to_onehot(nn_input)
    else:
        NotImplementedError

