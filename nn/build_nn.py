import keras
import keras.backend as K
import numpy as np
import tensorflow as tf
import pickle
from nn.loss_functions import *
from nn.Keras_WeightedZeroSumLayer import *
# import matplotlib.pyplot as plt


class HuberL1LinfHistory(keras.callbacks.Callback):
    def __init__(self):
        super(HuberL1LinfHistory, self).__init__()

    def on_train_begin(self, logs=None):
        self.val_losses = {}
        self.val_losses['val_huber_loss'] = []
        self.val_losses['val_l1_loss'] = []
        self.val_losses['val_linf_loss'] = []

    def on_batch_end(self, batch, logs=None):
        self.val_losses['val_huber_loss'].append(logs.get('val_huber_loss'))
        self.val_losses['val_l1_loss'].append(logs.get('val_l1_loss'))
        self.val_losses['val_linf_loss'].append(logs.get('val_linf_loss'))


class BatchCheckpoint(keras.callbacks.Callback):

    def __init__(self, filepath):
        super(BatchCheckpoint, self).__init__()
        self.huber = 'val_huber_loss'
        self.l1 = 'val_l1_loss'
        self.linf = 'val_linf_loss'
        self.verbose = 1
        self.filepath = filepath + '/GP{val_huber_loss:.4f}_{val_l1_loss:.4f}_{val_linf_loss:.4f}.h5'

    def on_batch_end(self, batch, logs=None):
        logs = logs or {}
        currenthuber = logs.get(self.huber)
        currentl1 = logs.get(self.l1)
        currentlinf = logs.get(self.linf)
        filepath = self.filepath.format(val_huber_loss=currenthuber, val_l1_loss=currentl1,
                                        val_linf_loss=currentlinf, **logs)
        self.model.save(filepath, overwrite=False)


def build_fnn(input_shape, output_shape, hidden_num, width_num, activation='relu', seed=1):
    model = keras.models.Sequential()
    model.add(keras.layers.Dense(input_dim=input_shape, units=input_shape * width_num, activation=activation))
    for i in range(hidden_num):
        model.add(keras.layers.Dense(units=input_shape * width_num, activation=activation,
                                     kernel_initializer=keras.initializers.he_uniform(seed=seed)))

    model.add(
        keras.layers.Dense(output_shape, activation='linear',
                           kernel_initializer=keras.initializers.he_uniform(seed=seed)))

    return model


def build_fnn_dynamic_loss(input_shape, output_shape, hidden_num, width_num, activation='relu', seed=1):
    input_tensor = keras.layers.Input(shape=(input_shape,))
    layer = keras.layers.Dense(input_dim=input_shape, units=input_shape * width_num, activation=activation)(
        input_tensor)
    for i in range(hidden_num):
        layer = keras.layers.Dense(units=input_shape * width_num, activation=activation,
                                   kernel_initializer=keras.initializers.he_uniform(seed=seed))(layer)

    out = keras.layers.Dense(output_shape, activation='linear',
                             kernel_initializer=keras.initializers.he_uniform(seed=seed))(layer)
    model = keras.models.Model(inputs=input_tensor, outputs=out)
    return model, input_tensor


def build_zerosum(input_shape, output_shape, width_num, activation, ps_num):
    input = keras.layers.Input(shape=(input_shape,))

    x1 = keras.layers.Dense(units=input_shape * width_num, activation=activation,
                            kernel_initializer=keras.initializers.he_uniform())(input)

    x2 = keras.layers.Dense(units=input_shape * width_num, activation=activation,
                            kernel_initializer=keras.initializers.he_uniform())(x1)

    x3 = keras.layers.Dense(units=input_shape * width_num, activation=activation,
                            kernel_initializer=keras.initializers.he_uniform())(x2)

    out = keras.layers.Dense(units=output_shape, activation="linear")(x3)

    zerosum = ZeroSumLayer(output_shape, ps_num)([out, input])

    model = keras.models.Model(inputs=input, outputs=zerosum)

    return model


def fnn_zerosumloss(input_shape, output_shape, width_num, ps_num, nn_input, nn_output, epoch_num, callback, multiplier):
    input = keras.layers.Input(shape=(input_shape,))

    x1 = keras.layers.Dense(units=input_shape * width_num, activation='relu',
                            kernel_initializer=keras.initializers.he_normal())(input)

    x2 = keras.layers.Dense(units=input_shape * width_num, activation='relu',
                            kernel_initializer=keras.initializers.he_normal())(x1)

    x3 = keras.layers.Dense(units=input_shape * width_num, activation='relu',
                            kernel_initializer=keras.initializers.he_normal())(x2)

    out = keras.layers.Dense(units=output_shape, activation="linear",
                             kernel_initializer=keras.initializers.he_normal())(
        x3)

    model = keras.models.Model(inputs=input, outputs=out)

    model.compile(optimizer=keras.optimizers.Adam(), loss=linf_loss,
                  metrics=[huber_loss, linf_loss, ZeroSumLoss(input, ps_num)])
    for mult in range(1, multiplier):
        model.fit(x=nn_input, y=nn_output, batch_size=mult * ps_num, epochs=epoch_num, validation_split=0.1,
                  callbacks=callback)

    return model


def randomnetwork(input_shape, output_shape, width_num, ps_num, nn_input, nn_output, epoch_num, callback, multiplier):
    input = keras.layers.Input(shape=(input_shape,))

    x1 = keras.layers.Dense(units=input_shape * width_num, activation='relu',
                            kernel_initializer=keras.initializers.he_normal())(input)

    x2 = keras.layers.Dense(units=input_shape * width_num, activation='relu',
                            kernel_initializer=keras.initializers.he_normal())(x1)

    x3 = keras.layers.Dense(units=input_shape * width_num, activation='relu',
                            kernel_initializer=keras.initializers.he_normal())(x2)

    out = keras.layers.Dense(units=output_shape, activation="linear",
                             kernel_initializer=keras.initializers.he_normal())(
        x3)

    model = keras.models.Model(inputs=input, outputs=out)

    model.compile(optimizer=keras.optimizers.Adam(0.00000001), loss=huber_loss,
                  metrics=[tf.losses.absolute_difference, linf_loss])
    # testloss = model.evaluate(nn_input,nn_output,verbose=1)
    # print("Testloss: ")
    # print(testloss)
    for i in range(4):
        model.fit(x=nn_input, y=nn_output, batch_size=87, epochs=1, validation_split=0.1, callbacks=callback)
        model.save("poker_random_verynew_" + str(i) + ".h5")

    return model


def weighted_linf_loss_network(input_shape, output_shape, width_num, ps_num, nn_input, nn_output, epoch_num=10,
                               callback=[], multiplier=5):
    input = keras.layers.Input(shape=(input_shape,))

    x1 = keras.layers.Dense(units=input_shape * width_num, activation='relu',
                            kernel_initializer=keras.initializers.he_normal())(input)

    x2 = keras.layers.Dense(units=input_shape * width_num, activation='relu',
                            kernel_initializer=keras.initializers.he_normal())(x1)

    x3 = keras.layers.Dense(units=input_shape * width_num, activation='relu',
                            kernel_initializer=keras.initializers.he_normal())(x2)

    out = keras.layers.Dense(units=output_shape, activation="linear",
                             kernel_initializer=keras.initializers.he_normal())(
        x3)

    model = keras.models.Model(inputs=input, outputs=out)

    model.compile(optimizer=keras.optimizers.Adam(), loss=reach_weighted_linf_loss(input, ps_num),
                  metrics=[huber_loss, linf_loss, ZeroSumLoss(input, ps_num)])
    testloss = model.evaluate(nn_input, nn_output, verbose=1)
    print("Testloss: ")
    print(testloss)
    for mult in range(1, multiplier):
        model.fit(x=nn_input, y=nn_output, batch_size=1, epochs=epoch_num, validation_split=0.1, callbacks=callback)

    return model


def fnn_kld_loss(input_shape, output_shape, width_num, ps_num, nn_input, nn_output, epoch_num=10, callback=[],
                 multiplier=5):
    input = keras.layers.Input(shape=(input_shape,))

    x1 = keras.layers.Dense(units=input_shape * width_num, activation='relu',
                            kernel_initializer=keras.initializers.he_uniform())(input)

    x2 = keras.layers.Dense(units=input_shape * width_num, activation='relu',
                            kernel_initializer=keras.initializers.he_uniform())(x1)

    x3 = keras.layers.Dense(units=input_shape * width_num, activation='relu',
                            kernel_initializer=keras.initializers.he_uniform())(x2)

    out = keras.layers.Dense(units=output_shape, activation="linear")(x3)

    model = keras.models.Model(inputs=input, outputs=out)

    model.compile(optimizer=keras.optimizers.Adam(), loss=KLD_CFV,
                  metrics=[huber_loss, linf_loss, ZeroSumLoss(input, ps_num)])
    for mult in range(1, multiplier):
        model.fit(x=nn_input, y=nn_output, batch_size=mult * ps_num, epochs=epoch_num, validation_split=0.1,
                  callbacks=callback)

    return model


def train_zerosum(model, nn_input, nn_output, train_loss, epoch_num, callack, multiplier, ps_num):
    model.compile(optimizer=keras.optimizers.Adam(), loss=train_loss, metrics=[linf_loss])
    for mult in range(1, multiplier):
        model.fit(x=nn_input, y=nn_output, batch_size=mult * ps_num, epochs=epoch_num, validation_split=0.1,
                  callbacks=callack)

    return model


def build_preluzerosum(input_shape, output_shape, width_num, ps_num):
    input = keras.layers.Input(shape=(input_shape,))

    x1 = keras.layers.Dense(units=input_shape * width_num, activation='linear',
                            kernel_initializer=keras.initializers.he_uniform())(input)

    prelu1 = keras.layers.PReLU()(x1)

    x2 = keras.layers.Dense(units=input_shape * width_num, activation='linear',
                            kernel_initializer=keras.initializers.he_uniform())(prelu1)

    prelu2 = keras.layers.PReLU()(x2)

    x3 = keras.layers.Dense(units=input_shape * width_num, activation='linear',
                            kernel_initializer=keras.initializers.he_uniform())(prelu2)

    prelu3 = keras.layers.PReLU()(x3)

    out = keras.layers.Dense(units=output_shape, activation="linear",
                             kernel_initializer=keras.initializers.glorot_uniform())(prelu3)

    preluout = keras.layers.PReLU()(out)

    zerosum = ZeroSumLayer(output_shape, ps_num)([preluout, input])

    model = keras.models.Model(inputs=input, outputs=zerosum)

    return model


def train_preluzerosum(model, nn_input, nn_output, train_loss, epoch_num, callack, multiplier, ps_num):
    model.compile(optimizer=keras.optimizers.Adam(), loss=train_loss, metrics=[linf_loss])
    for mult in range(1, multiplier):
        model.fit(x=nn_input, y=nn_output, batch_size=mult * ps_num, epochs=epoch_num, validation_split=0.1,
                  callbacks=callack)

    return model


def train_fnn2(model, nn_input, nn_output, nn_input_val, nn_output_val, train_loss, epoch_num, callack):
    model.compile(optimizer=keras.optimizers.Adam(), loss=train_loss, metrics=[linf_loss])
    hist = model.fit(x=nn_input, y=nn_output, batch_size=100, epochs=epoch_num,
                     validation_data=(nn_input_val, nn_output_val), callbacks=callack)
    return hist.history


def train_fnn(model, nn_input, nn_output, train_loss, epoch_num, callack):
    model.compile(optimizer=keras.optimizers.Adam(), loss=train_loss, metrics=[huber_loss, linf_loss])
    model.fit(x=nn_input, y=nn_output, batch_size=100, epochs=epoch_num, callbacks=callack, validation_split=0.1)
    return model


def train_L1LowLR(model, nn_input, nn_output, LR, filepath):
    model.compile(optimizer=keras.optimizers.Adam(LR), loss=huber_loss,
                  metrics=[huber_loss, l1_loss, linf_loss, l0_loss])
    model.fit(x=nn_input, y=nn_output, batch_size=100, epochs=1, validation_split=0.1,
              callbacks=[HuberL1LinfHistory(), BatchCheckpoint(filepath)])
    return model


def train_AbsSumFnn(model, nn_input, nn_output, train_loss, epoch_num, callack):
    model.compile(optimizer=keras.optimizers.Adam(), loss=AbsSumLoss, metrics=[huber_loss, linf_loss])
    model.fit(x=nn_input, y=nn_output, batch_size=100, epochs=epoch_num, callbacks=callack, verbose=0)
    return model


def save_pkl(dict, file_path):
    f = open(file_path, "wb")
    pickle.dump(dict, f)
    f.close()


# def networkzerosum(modelpath, input, ps_num):
# 	if isinstance(modelpath, str):
#
# 		model = keras.models.load_model(modelpath,
# 		                                custom_objects={"huber_loss": huber_loss, "linf_loss": linf_loss, "ZeroSumLayer":
# 			                                ZeroSumLayer})
# 	else:
# 		model = modelpath
# 	zerosum = []
# 	x = np.arange(0, input.shape[0])
# 	for row in range(input.shape[0]):
# 		preds = model.predict(np.reshape(input.values[row, :], newshape=(1, input.shape[1])))
# 		weightedsum = np.sum(np.multiply(preds, input.values[row, ps_num:]))
# 		zerosum.append(weightedsum)
# 	mse = np.mean(np.square(np.array(zerosum) - np.mean(zerosum)))
# 	print("Network has average zerosum error of {}".format(mse))
# 	plt.plot(x, np.array(zerosum))
# 	plt.show()
