import pandas as pd
import os
from numpy import array

#gs3
numPossibleHandsgs3t2 = 3
numPossibleHandsgs3t4 = 6

numPublicFeaturesgs3t2 = 3
numPublicFeaturesgs3t4 = 6

#gs4
numPossibleHandsgs4t2 = 4
numPossibleHandsgs4t4 = 12
numPossibleHandsgs4t6 = 24

numPublicFeaturesgs4t2 = 3
numPublicFeaturesgs4t4 = 6
numPublicFeaturesgs4t6 = 9

## gs5
numPossibleHandsgs5t4 = 20

numPublicFeaturesgs5t4 = 5


def getDomainConstantsFor(domain):
    assert isinstance(domain, str)

    if domain == "gs3t2":
        return numPossibleHandsgs3t2,numPublicFeaturesgs3t2

    elif domain == "gs3t4":
        return numPossibleHandsgs3t4, numPublicFeaturesgs3t4

    elif domain == "gs4t2":
        return numPossibleHandsgs4t2, numPublicFeaturesgs4t2

    elif domain == "gs4t4":
        return numPossibleHandsgs4t4, numPublicFeaturesgs4t4

    elif domain == "gs4t6":
        return numPossibleHandsgs4t6, numPublicFeaturesgs4t6

    elif domain == "gs5t4":
        return numPossibleHandsgs5t4, numPublicFeaturesgs5t4
    else:
        NotImplementedError

#domain loading

def load_data_for_domain(domain):
    assert isinstance(domain,str)

    nn_input, nn_output = None,None

    if domain == "gs3t2":
        nn_input = pd.read_csv("/home/dominik/Desktop/cpp_seeds/IIGS3T2_random_input.csv")
        nn_output = pd.read_csv("/home/dominik/Desktop/cpp_seeds/IIGS3T2_random_output.csv")

    elif domain == "gs3t4":
        nn_input = pd.read_csv("/home/dominik/Desktop/cpp_seeds/IIGS3T4_random_input.csv")
        nn_output = pd.read_csv("/home/dominik/Desktop/cpp_seeds/IIGS3T4_random_output.csv")

    elif domain == "gs4t2":
        nn_input = pd.read_csv("/home/dominik/Desktop/cpp_seeds/gs4/gs4t2/IIGS4T2_random_input.csv")
        nn_output = pd.read_csv("/home/dominik/Desktop/cpp_seeds/gs4/gs4t2/IIGS4T2_random_output.csv")

    elif domain == "gs4t4":
        nn_input = pd.read_csv("/home/dominik/Desktop/cpp_seeds/gs4/gs4t4/IIGS4T4_random_input.csv")
        nn_output = pd.read_csv("/home/dominik/Desktop/cpp_seeds/gs4/gs4t4/IIGS4T4_random_output.csv")

    elif domain == "gs4t6":
        nn_input = pd.read_csv("/home/dominik/Desktop/cpp_seeds/gs4/gs4t6/IIGS4T6_random_input.csv")
        nn_output = pd.read_csv("/home/dominik/Desktop/cpp_seeds/gs4/gs4t6/IIGS4T6_random_output.csv")

    elif domain == "gs5t4":
        nn_input = pd.read_csv("data/data_iigs5t4/input_gs5_trunk4.csv")
        nn_output = pd.read_csv("data/data_iigs5t4/output_gs5_trunk4.csv")
    else:
        NotImplementedError

    print('data for domain {} loaded'.format(domain))
    assert nn_input is not None and nn_output is not None
    return array(nn_input),array(nn_output)