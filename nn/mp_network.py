import keras
import numpy as np
import pandas as pd
import random

##

player = 0

rangesheads = [random.random() for i in range(10)]
rangestails = [1 - rangesheads[i] for i in range(len(rangesheads))]

headsactionencoded = [1,0]
tailactionencoded = [0,1]


headsauginfset = [0,1,0.5,1,0,0.5,0]

tailsauginfset = [1,0,0.5,0,1,0.5,0]

def createtrainingdata(listranges,listvalues):
    assert len(listranges)==len(listvalues)
    numtrainingsamples = len(listranges)

    mytrain = np.array()

    for i in range(listranges):
        if i % 2 == 0:

            mysample = np.array(rangesheads+listranges[i]+rangestails+listranges[i]+player)
        else:
            mysample = np.array(rangestails + listranges[i] + rangesheads + listranges[i] + player)

        mytrain = np.concatenate([mytrain,mysample],axis=0)


    inputtraining,outputtraining = None
    return inputtraining,outputtraining
