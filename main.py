from nn.build_nn import *
from nn.loss_functions import *
import keras
import numpy as np
import pandas as pd

## TODO @Matej: The general procedure of training & evaluating networks' exploitability involves a couple of steps:
## TODO 1. in GTLib2: use the main_generate_data.cpp in c++ to generate seeds (provide domain+domain settings)
## TODO 2. This generates CSV files which you then convert to training data with SeedsToNNInput.py
## TODO 3. Afterwards use build_nn.py to train the networks with that created training data
## TODO 4. Save them using the procedures described in build_nn.py
## TODO 5. Then use keras_to_tensorflow.py to convert the saved .h5 to .pb format to be able to load them in c++
## TODO 6. in the main_cluster.cpp you'll find methods called something like double LossVsExpl()
## TODO They require a string with the path to a network you want to compute exploitability with
## TODO Our goal is to measure the relationship between a networks loss and the exploitability of
## TODO depth-limited CFR using that networks to solve the subgames.
## TODO So we usually evaluate a list of networks and then plot their losses with their corresponding expl.


# TODO 1. Train networks with reach_weighted_linf, reach_weighted_l1 and reach_weighted_huber and evaluate exploitability
# TODO 2. Do the same with the game value of the public state instead of the reach.
# TODO 3. Analyze how sparseness of the seeds affects lossvsExpl
# TODO 4. Implement Kriegspiel

numPossibleHands = 12
numPublicFeatures = 5
input_shape = 2 * numPossibleHands + numPublicFeatures
output_shape = 2 * numPossibleHands
n_hidden = 2
width_num = 10

seed = 1
activation = 'relu'

nn_input = pd.read_csv('/home/matej/Desktop/gs4_t4/gs4_t4_seeds/IIGS4T4_random_input.csv')
nn_output = pd.read_csv('/home/matej/Desktop/gs4_t4/gs4_t4_seeds/IIGS4T4_random_output.csv')

nn_input_train = nn_input.iloc[:-180, :]
nn_output_train = nn_output.iloc[:-180, :]

nn_input_val = nn_input.iloc[-180:, :]
nn_output_val = nn_output.iloc[-180:, :]

batchsize = 100
epochs = 100

input_tensor = keras.layers.Input(shape=(input_shape,))
layer = keras.layers.Dense(input_dim=input_shape, units=input_shape * width_num, activation=activation)(
        input_tensor)
for i in range(n_hidden):
    layer = keras.layers.Dense(units=input_shape * width_num, activation=activation,
                               kernel_initializer=keras.initializers.he_uniform(seed=seed))(layer)

out = keras.layers.Dense(output_shape, activation='linear',
                         kernel_initializer=keras.initializers.he_uniform(seed=seed))(layer)
model = keras.models.Model(inputs=input_tensor, outputs=out)

# model = build_fnn(input_shape, output_shape, n_hidden, width_num, activation, seed)
model.summary()
# model.compile(optimizer=keras.optimizers.Adam(0.001), loss=huber_loss)

# model, input_ = build_fnn_dynamic_loss(input_shape, output_shape, n_hidden, width_num, activation, seed)
# model.summary()
metrics = [reach_weighted_huber(PubStateRanges=input_tensor, numPubFeatures=numPublicFeatures),
           reach_weighted_l1(PubStateRanges=input_tensor, numPubFeatures=numPublicFeatures),
           reach_weighted_linf(PubStateRanges=input_tensor, numPubFeatures=numPublicFeatures), l0_loss]

model.compile(optimizer=keras.optimizers.Adam(0.001), loss=reach_weighted_huber(PubStateRanges=input_tensor, numPubFeatures=numPublicFeatures), metrics=metrics)
# CSVLogger = keras.callbacks.CSVLogger('/home/matej/Desktop/gs4_t4' + "/IIGS4T4" + "_losses" + ".csv")

model.fit(x=nn_input_train, y=nn_output_train, batch_size=batchsize, epochs=epochs, verbose=1,
          validation_data=[nn_input_val, nn_output_val])
model.save("/home/matej/Desktop/gs4_t4/gs4t4_reach_weighted.h5")
