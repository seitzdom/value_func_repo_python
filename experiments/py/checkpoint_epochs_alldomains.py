#!/usr/bin/python
from nn.build_nn import *
import pandas as pd
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("--networkpath", type=str)
parser.add_argument("--datapath", type=str)
parser.add_argument("--domain", type=str)
parser.add_argument("--batchsize", type=int)
parser.add_argument("--epochs", type=int)
parser.add_argument("--lr", type=float)
parser.add_argument("--reachweight", type=bool, default=False)

args = parser.parse_args()


def main(args):
    networkpath = args.networkpath
    datapath = args.datapath
    domain = args.domain
    batchsize = args.batchsize
    epochs = args.epochs
    lr = args.lr
    reachweight = args.reachweight

    actionseq = None
    ps_num = None
    input_shape = None
    output_shape = None
    n_hidden = None
    width_num = None
    seed = None
    nn_input = None
    nn_output = None

    if domain == "GP322221":
        print("GP322221")
        actionseq = 3
        ps_num = 27
        input_shape = 2 * actionseq + ps_num
        output_shape = 2 * actionseq
        n_hidden = 2
        width_num = 10
        seed = 1
        nn_input = pd.read_csv(datapath + '/data_poker/input_poker.csv')
        nn_output = pd.read_csv(datapath + '/data_poker/output_poker.csv')


    elif domain == "IIGS5T4":
        print("IIGS5T4")
        actionseq = 20
        ps_num = 5
        input_shape = 2 * actionseq + ps_num
        output_shape = 2 * actionseq
        n_hidden = 2
        width_num = 10
        seed = 1
        nn_input = pd.read_csv(datapath + '/data_iigs5t4/input_gs5_trunk4.csv')
        nn_output = pd.read_csv(datapath + '/data_iigs5t4/output_gs5_trunk4.csv')
        batchsize = 100

    elif domain == "OZ8C3B":
        print("OZ8C3B")
        actionseq = 35
        ps_num = 7
        input_shape = 2 * actionseq + ps_num
        output_shape = 2 * actionseq
        n_hidden = 2
        width_num = 10
        seed = 1
        nn_input = pd.read_csv(datapath + "/data_OZ8CB3/input_osh_c8l1_trunk6_2000.csv")
        nn_output = pd.read_csv(datapath + "/data_OZ8CB3/output_osh_c8l1_trunk6_2000.csv")

    elif domain == "IIGS4T4":
        print("IIGS4T4")
        actionseq = 12
        ps_num = 5
        input_shape = 2 * actionseq + ps_num
        output_shape = 2 * actionseq
        n_hidden = 2
        width_num = 10
        seed = 1
        nn_input = pd.read_csv(datapath + '/IIGS4T4_random_input.csv')
        nn_output = pd.read_csv(datapath + '/IIGS4T4_random_output.csv')

    if reachweight:
        model, input_tensor = build_fnn_dynamic_loss(input_shape, output_shape, n_hidden, width_num, "relu", seed)
        metrics = [reach_weighted_huber(input_tensor, ps_num), reach_weighted_l1(input_tensor, ps_num), reach_weighted_linf(input_tensor, ps_num)]
        loss = reach_weighted_huber(input_tensor, ps_num)
    else:
        model = build_fnn(input_shape, output_shape, n_hidden, width_num, "relu", seed)
        metrics = [huber_loss, l1_loss, linf_loss, l0_loss]
        loss = huber_loss

    model.compile(optimizer=keras.optimizers.Adam(lr), loss=loss, metrics=metrics)

    EpochCheckPoint = keras.callbacks.ModelCheckpoint(networkpath + "/" + domain + "_" + "{epoch}.h5",
                                                      monitor="val_linf_loss", save_best_only=False, mode="min",
                                                      period=1)
    CSVLogger = keras.callbacks.CSVLogger(networkpath + "/" + domain + "_losses" + ".csv")

    model.fit(x=nn_input, y=nn_output, batch_size=batchsize, epochs=epochs, verbose=1,
              callbacks=[EpochCheckPoint, CSVLogger], validation_split=0.1)

    del model
    K.clear_session()


if __name__ == "__main__":
    main(args)
