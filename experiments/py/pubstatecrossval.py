import pandas as pd
from nn.build_nn import *
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("--domain", type=str)
args = parser.parse_args()

##TODO weigh linf error by histores. or order pubstates in plot by size
#TODO generate one seed for each domain. count and save numbers

def filter_by_public_state(nn_input=None,nn_output=None,public_state=None,domain=None):
	filtered_nn_input=None
	filtered_nn_output=None
	val_in=None
	val_out=None

	nn_input_copy = nn_input.copy()
	nn_output_copy = nn_output.copy()
	public_state = public_state.tolist()
	if domain=="IIGS5T4":
		indices = nn_input_copy[(nn_input_copy.iloc[:,0]==public_state[0]) & (nn_input_copy.iloc[:,1]==public_state[1])& (nn_input_copy.iloc[:,2]==public_state[2])&
	                   (nn_input_copy.iloc[:,3]==public_state[3])&(nn_input_copy.iloc[:,4]==public_state[4])].index.values.tolist()
		val_in = nn_input_copy.iloc[indices,:]
		val_out = nn_output_copy.iloc[indices,:]
		filtered_nn_input= nn_input.drop(indices,inplace=False)
		filtered_nn_output = nn_output.drop(indices,inplace=False)
	elif domain=="OZ8C3B":
		indices = nn_input_copy[
			(nn_input_copy.iloc[:, 0] == public_state[0]) & (nn_input_copy.iloc[:, 1] == public_state[1]) & (
					nn_input_copy.iloc[:, 2] == public_state[2]) &
			(nn_input_copy.iloc[:, 3] == public_state[3]) & (
					nn_input_copy.iloc[:, 4] == public_state[4]) & (
					nn_input_copy.iloc[:, 5] == public_state[5]) & (
					nn_input_copy.iloc[:, 6] == public_state[6])].index.values.tolist()
		val_in = nn_input_copy.iloc[indices, :]
		val_out = nn_output_copy.iloc[indices, :]
		filtered_nn_input = nn_input.drop(indices, inplace=False)
		filtered_nn_output = nn_output.drop(indices, inplace=False)

	elif domain=="GP322221":
		indices = nn_input_copy[
			(nn_input_copy.iloc[:, 0] == public_state[0]) & (nn_input_copy.iloc[:, 1] == public_state[1]) & (
					nn_input_copy.iloc[:, 2] == public_state[2]) &
			(nn_input_copy.iloc[:, 3] == public_state[3]) & (
					nn_input_copy.iloc[:, 4] == public_state[4]) & (
					nn_input_copy.iloc[:, 5] == public_state[5]) &

				(nn_input_copy.iloc[:, 6] == public_state[6]) &
			(nn_input_copy.iloc[:, 7] == public_state[7]) &
			(nn_input_copy.iloc[:, 8] == public_state[8]) &
			(nn_input_copy.iloc[:, 9] == public_state[9]) &
			(nn_input_copy.iloc[:, 10] == public_state[10]) &
			(nn_input_copy.iloc[:, 11] == public_state[11]) &
			(nn_input_copy.iloc[:, 12] == public_state[12]) &
			(nn_input_copy.iloc[:, 13] == public_state[13]) &
			(nn_input_copy.iloc[:, 14] == public_state[14]) &
			(nn_input_copy.iloc[:, 15] == public_state[15]) &
			(nn_input_copy.iloc[:, 16] == public_state[16]) &
			(nn_input_copy.iloc[:, 17] == public_state[17]) &
			(nn_input_copy.iloc[:, 18] == public_state[18]) &
			(nn_input_copy.iloc[:, 19] == public_state[19]) &
			(nn_input_copy.iloc[:, 20] == public_state[20]) &
			(nn_input_copy.iloc[:, 21] == public_state[21]) &
			(nn_input_copy.iloc[:, 22] == public_state[22]) &
			(nn_input_copy.iloc[:, 23] == public_state[23]) &
			(nn_input_copy.iloc[:, 24] == public_state[24]) &
			(nn_input_copy.iloc[:, 25] == public_state[25]) &
			(nn_input_copy.iloc[:, 26] == public_state[26])

			].index.values.tolist()
		val_in = nn_input_copy.iloc[indices, :]
		val_out = nn_output_copy.iloc[indices, :]
		filtered_nn_input = nn_input.drop(indices, inplace=False)
		filtered_nn_output = nn_output.drop(indices, inplace=False)
	return filtered_nn_input,filtered_nn_output,val_in,val_out



def PubStateCrossVal(domain):
	datapath="data"
	actionseq = None
	ps_num = None
	input_shape = None
	output_shape = None
	n_hidden = None
	width_num = None
	seed = 1
	nn_input = None
	nn_output = None
	filterin=None
	filterout=None
	val_in=None
	val_out =None
	epochs=200

	if domain=="GP322221":
		print("GP322221")
		actionseq = 3
		ps_num = 27
		input_shape = 2 * actionseq + ps_num
		output_shape = 2 * actionseq
		n_hidden = 2
		width_num = 10
		seed = 1
		nn_input = pd.read_csv(datapath + '/data_poker/input_poker.csv')
		nn_output = pd.read_csv(datapath + '/data_poker/output_poker.csv')
		epochs=100

	elif domain == "IIGS5T4":
		print("IIGS5T4")
		actionseq = 20
		ps_num = 5
		input_shape = 2 * actionseq + ps_num
		output_shape = 2 * actionseq
		n_hidden = 2
		width_num = 10
		seed = 1
		nn_input = pd.read_csv(datapath + '/data_iigs5t4/input_gs5_trunk4.csv')
		nn_output = pd.read_csv(datapath + '/data_iigs5t4/output_gs5_trunk4.csv')
		batchsize=100

	elif domain == "OZ8C3B":
		print("OZ8C3B")
		actionseq = 35
		ps_num = 7
		input_shape = 2 * actionseq + ps_num
		output_shape = 2 * actionseq
		n_hidden = 2
		width_num = 10
		seed = 1
		nn_input = pd.read_csv(datapath + '/data_OZ8CB3/input_osh_c8l1_trunk6_2000.csv')
		nn_output = pd.read_csv(datapath + '/data_OZ8CB3/output_osh_c8l1_trunk6_2000.csv')

	pubstatedf = nn_input.iloc[:,:ps_num].drop_duplicates()

	for pubstate in range(pubstatedf.shape[0]):
		CSVLogger = keras.callbacks.CSVLogger("experiment_results/publicstate_crossval/" + domain +"_publicstate_"+ str(pubstate) +"_losses" + ".csv")
		filterin,filterout,val_in,val_out = filter_by_public_state(nn_input,nn_output,pubstatedf.iloc[pubstate,:],domain)
		model = build_fnn(input_shape, output_shape, n_hidden, width_num, "relu", seed)
		model.compile(optimizer=keras.optimizers.Adam(0.001), loss=huber_loss,
		              metrics=[huber_loss, l1_loss, linf_loss, l0_loss])
		model.fit(x=filterin, y=filterout, batch_size=100, epochs=epochs, verbose=1,validation_data=[val_in,val_out],callbacks=[CSVLogger])
		del model
		keras.backend.clear_session()


def main(args):
	PubStateCrossVal(args.domain)

if __name__ == "__main__":
	main(args)


