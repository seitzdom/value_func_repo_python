#!/usr/bin/python
from nn.build_nn import *
import pandas as pd
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("--domain", type=str)
args = parser.parse_args()


def main(args):
    domain = args.domain

    actionseq = None
    ps_num = None
    input_shape = None
    output_shape = None
    nn_input = None
    nn_output = None
    nn_input_train = None
    nn_output_train = None
    nn_input_val = None
    nn_output_val = None

    if domain == "GP322221":
        print(domain)
        actionseq = 3
        ps_num = 27
        input_shape = 2 * actionseq + ps_num
        output_shape = 2 * actionseq
        nn_input = pd.read_csv('data/data_poker/input_poker.csv')
        nn_output = pd.read_csv('data/data_poker/output_poker.csv')

        nn_input_train = nn_input.iloc[:-1000, :]
        nn_output_train = nn_output.iloc[:-1000, :]

        nn_input_val = nn_input.iloc[-1000:, :]
        nn_output_val = nn_output.iloc[-1000:, :]

    elif domain == "IIGS5T4":
        print(domain)
        actionseq = 20
        ps_num = 5
        input_shape = 2 * actionseq + ps_num
        output_shape = 2 * actionseq

        nn_input = pd.read_csv('data/data_iigs5t4/input_gs5_trunk4.csv')
        nn_output = pd.read_csv('data/data_iigs5t4/output_gs5_trunk4.csv')

        nn_input_train = nn_input.iloc[:-1000, :]
        nn_output_train = nn_output.iloc[:-1000, :]

        nn_input_val = nn_input.iloc[-1000:, :]
        nn_output_val = nn_output.iloc[-1000:, :]

    elif domain == "OZ8C3B":
        print("OZ8C3B")
        actionseq = 35
        ps_num = 7
        input_shape = 2 * actionseq + ps_num
        output_shape = 2 * actionseq

        nn_input = pd.read_csv('data/data_OZ8CB3/input_osh_c8l1_trunk6_2000.csv')
        nn_output = pd.read_csv('data/data_OZ8CB3/output_osh_c8l1_trunk6_2000.csv')

        nn_input_train = nn_input.iloc[1000:, :]
        nn_output_train = nn_output.iloc[1000:, :]

        nn_input_val = nn_input.iloc[:1000, :]
        nn_output_val = nn_output.iloc[:1000, :]

    seed = 1

    # Depth

    for n_hidden in range(1, 10):
        CSVLogger = keras.callbacks.CSVLogger("hyp_opt/Depth/" + domain + "_HO_Nhidden_" + str(n_hidden) + ".csv")
        width_num = 2

        model = build_fnn(input_shape, output_shape, n_hidden, width_num, "relu", seed)
        model.compile(optimizer=keras.optimizers.Adam(0.001), loss=huber_loss,
                      metrics=[huber_loss, l1_loss, linf_loss, l0_loss])

        hist = model.fit(x=nn_input_train, y=nn_output_train, batch_size=100, epochs=500, verbose=0,
                         validation_data=[nn_input_val, nn_output_val], callbacks=[CSVLogger])

        del model
        K.clear_session()

    # # Width
    #
    # for width_num in range(1, 8):
    #     CSVLogger = keras.callbacks.CSVLogger("hyp_opt/Width/" + domain + "_HO_Width_" + str(width_num) + ".csv")
    #     n_hidden = 3
    #     model = build_fnn(input_shape, output_shape, n_hidden, width_num, "relu", seed)
    #     model.compile(optimizer=keras.optimizers.Adam(0.001), loss=huber_loss,
    #                   metrics=[huber_loss, l1_loss, linf_loss, l0_loss])
    #
    #     hist = model.fit(x=nn_input_train, y=nn_output_train, batch_size=100, epochs=200, verbose=0,
    #                      validation_data=[nn_input_val, nn_output_val], callbacks=[CSVLogger])
    #
    #     del model
    #     K.clear_session()

    # Datanum
    ##TODO make the first 1000 the val data so it stays the same when increasing data amount DONE
    # for datanum in range(31000, 50000, 1000):
    #     CSVLogger = keras.callbacks.CSVLogger(
    #         "hyp_opt/DataAmount/" + domain + "_HO_DataAmount_" + str(datanum) + ".csv")
    #     width_num = 10
    #     n_hidden = 3
    #     model = build_fnn(input_shape, output_shape, n_hidden, width_num, "relu", seed)
    #     model.compile(optimizer=keras.optimizers.Adam(0.001), loss=huber_loss,
    #                   metrics=[huber_loss, l1_loss, linf_loss, l0_loss])
    #
    #     nn_input_train_num = nn_input_train.iloc[:datanum, :]
    #     nn_output_train_num = nn_output_train.iloc[:datanum, :]
    #
    #     hist = model.fit(x=nn_input_train_num, y=nn_output_train_num, batch_size=100, epochs=1000, verbose=0,
    #                      validation_data=[nn_input_val, nn_output_val], callbacks=[CSVLogger])
    #
    #     del model
    #     K.clear_session()

    # train on different losses

    # for loss in [huber_loss, l1_loss, linf_loss]:
    #     lossname = ""
    #     if loss == huber_loss:
    #         lossname = "huber"
    #     elif loss == l1_loss:
    #         lossname = "l1"
    #     elif loss == linf_loss:
    #         lossname = "linf"
    #
    #     CSVLogger = keras.callbacks.CSVLogger("hyp_opt/Loss/" + domain + "_HO_" + lossname + ".csv")
    #     width_num = 10
    #     n_hidden = 3
    #     model = build_fnn(input_shape, output_shape, n_hidden, width_num, "relu", seed)
    #     model.compile(optimizer=keras.optimizers.Adam(0.001), loss=loss,
    #                   metrics=[huber_loss, l1_loss, linf_loss, l0_loss])
    #
    #     hist = model.fit(x=nn_input_train, y=nn_output_train, batch_size=100, epochs=200, verbose=0,
    #                      validation_data=[nn_input_val, nn_output_val], callbacks=[CSVLogger])
    #
    #     del model
    #     K.clear_session()


if __name__ == "__main__":
    main(args)
