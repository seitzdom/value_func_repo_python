#!/usr/bin/python
import sys
sys.path.append('/home/matej/gt/python/value_func_repo_python')
from nn import build_nn
from nn.loss_functions import *
import pandas as pd
import numpy as np
from argparse import ArgumentParser
import csv
import random

parser = ArgumentParser()
parser.add_argument("--datapath", type=str)
parser.add_argument("--networkpath", type=str)
parser.add_argument("--domain", type=str)
parser.add_argument("--maxnumnetworks", type=int)
parser.add_argument("--batchsize", type=int)
parser.add_argument("--reachweight", type=bool, default=False)
parser.add_argument("--learningrate", type=float, default=0.001)

args = parser.parse_args()


def main(args):
    datapath = args.datapath
    networkpath = args.networkpath
    domain = args.domain
    maxnumnetworks = args.maxnumnetworks
    batchsize = args.batchsize
    reachweight = args.reachweight
    learningrate = args.learningrate
    print("Init values: maxnetworks {}, domain {}, batchsize {}".format(maxnumnetworks, domain, batchsize))

    if domain == "GP322221":
        print("GP322221")
        actionseq = 3
        ps_num = 27
        input_shape = 2 * actionseq + ps_num
        output_shape = 2 * actionseq
        n_hidden = 2
        width_num = 10
        seed = maxnumnetworks
        nn_input = pd.read_csv(datapath + '/data_poker/input_poker.csv')
        nn_output = pd.read_csv(datapath + '/data_poker/output_poker.csv')
        batchlosstolerance = 0.005
        firstepochtolerance = 0.003
        secondepochtolerance = 0.001

    elif domain == "IIGS5T4":
        print("IIGS5T4")
        actionseq = 20
        ps_num = 5
        input_shape = 2 * actionseq + ps_num
        output_shape = 2 * actionseq
        n_hidden = 2
        width_num = 10
        seed = maxnumnetworks
        nn_input = pd.read_csv(datapath + '/data_iigs5t4/input_gs5_trunk4.csv')
        nn_output = pd.read_csv(datapath + '/data_iigs5t4/output_gs5_trunk4.csv')
        batchlosstolerance = 0.005
        firstepochtolerance = 0.001
        secondepochtolerance = 0.001
        batchsize = 100

    elif domain == "OZ8C3B":
        print("OZ8C3B")
        actionseq = 35
        ps_num = 7
        input_shape = 2 * actionseq + ps_num
        output_shape = 2 * actionseq
        n_hidden = 2
        width_num = 10
        seed = maxnumnetworks
        nn_input = pd.read_csv(datapath + '/data_OZ8CB3/input_osh_c8l1_trunk6_2000.csv')
        nn_output = pd.read_csv(datapath + '/data_OZ8CB3/output_osh_c8l1_trunk6_2000.csv')

    elif domain == "IIGS4T4":
        print("IIGS4T4")
        actionseq = 12
        ps_num = 5
        input_shape = 2 * actionseq + ps_num
        output_shape = 2 * actionseq
        n_hidden = 2
        width_num = 10
        seed = maxnumnetworks
        nn_input = pd.read_csv(datapath + '/IIGS4T4_random_input.csv')
        nn_output = pd.read_csv(datapath + '/IIGS4T4_random_output.csv')
        batchlosstolerance = 0.005
        firstepochtolerance = 0.001
        secondepochtolerance = 0.001

    if domain == "IIGS4T4":
        nn_input_train = nn_input.iloc[:-200, :]
        nn_output_train = nn_output.iloc[:-200, :]

        nn_input_val = nn_input.iloc[-200:, :]
        nn_output_val = nn_output.iloc[-200:, :]

    else:
        nn_input_train = nn_input.iloc[:-1000, :]
        nn_output_train = nn_output.iloc[:-1000, :]

        nn_input_val = nn_input.iloc[-1000:, :]
        nn_output_val = nn_output.iloc[-1000:, :]

    if reachweight:
        model, input_ = build_nn.build_fnn_dynamic_loss(input_shape, output_shape, n_hidden, width_num, "relu", seed)
        metrics = [reach_weighted_huber(PubStateRanges=input_, numPubFeatures=ps_num),
                   reach_weighted_l1(PubStateRanges=input_, numPubFeatures=ps_num),
                   reach_weighted_linf(PubStateRanges=input_, numPubFeatures=ps_num), l0_loss]
        loss = reach_weighted_huber(PubStateRanges=input_, numPubFeatures=ps_num)

    else:
        model = build_nn.build_fnn(input_shape, output_shape, n_hidden, width_num, "relu", seed)
        metrics = [huber_loss, l1_loss, linf_loss, l0_loss]
        loss = huber_loss


    model.compile(optimizer=keras.optimizers.Adam(learningrate), loss=loss, metrics=metrics)
    vallosses = model.evaluate(x=nn_input_val, y=nn_output_val, batch_size=batchsize, verbose=0)
    huber = np.round(vallosses[1], decimals=4)
    l1 = np.round(vallosses[2], decimals=4)
    l_inf = np.round(vallosses[3], decimals=4)
    # model.save(networkpath + "GP_Hu_" + str(huber) + "_L1_" + str(l1) + "_Linf_" + str(l_inf) + ".h5")
    model.save(networkpath + "/" + domain + "_" + str(0) + ".h5")
    with open(networkpath + "/" + domain + "_maxnumnetworks_" + str(maxnumnetworks) + "_batchsize_" + str(
            batchsize) + "_losses.csv", "w") as file:
        writer = csv.writer(file, delimiter=",")
        writer.writerow([0, huber, l1, l_inf])
    print("#" + str(0) + " of " + str(maxnumnetworks) + " : latest:" + str(l_inf) + "_current:" + str(
        l_inf))
    latestlinf = l_inf

    numofsavednetworks = 1

    if domain == "GP32221":

        while numofsavednetworks <= maxnumnetworks - 10:

            batchindex = random.randint(0, nn_input_train.shape[0] - batchsize)

            model.train_on_batch(x=nn_input_train.iloc[batchindex:batchindex + batchsize, :],
                                 y=nn_output_train.iloc[batchindex:batchindex + batchsize, :])
            vallosses = model.evaluate(x=nn_input_val, y=nn_output_val, batch_size=batchsize, verbose=0)
            huber = np.round(vallosses[1], decimals=4)
            l1 = np.round(vallosses[2], decimals=4)
            l_inf = np.round(vallosses[3], decimals=4)
            if l_inf + batchlosstolerance <= latestlinf:
                print("#" + str(numofsavednetworks) + " of " + str(maxnumnetworks) + " : latest:" + str(
                    latestlinf) + "_current:" + str(l_inf))
                latestlinf = l_inf
                errors = [str(numofsavednetworks), str(huber), str(l1), str(l_inf)]
                with open(networkpath + "/" + domain + "_maxnumnetworks_" + str(maxnumnetworks) + "_batchsize_" + str(
                        batchsize) + "_losses.csv", "a") as file:
                    writer = csv.writer(file, delimiter=",")
                    writer.writerow(errors)

                model.save(networkpath + "/" + domain + "_" + str(numofsavednetworks) + ".h5")
                numofsavednetworks += 1

        while numofsavednetworks <= maxnumnetworks - 5:
            model.fit(x=nn_input_train, y=nn_output_train, batch_size=batchsize, epochs=1, verbose=0)
            vallosses = model.evaluate(x=nn_input_val, y=nn_output_val, batch_size=batchsize, verbose=0)
            huber = np.round(vallosses[1], decimals=4)
            l1 = np.round(vallosses[2], decimals=4)
            l_inf = np.round(vallosses[3], decimals=4)
            if l_inf + firstepochtolerance <= latestlinf:
                print("#" + str(numofsavednetworks) + " of " + str(maxnumnetworks) + " : latest:" + str(
                    latestlinf) + "_current:" + str(l_inf))
                latestlinf = l_inf
                errors = [str(numofsavednetworks), str(huber), str(l1), str(l_inf)]
                with open(networkpath + "/" + domain + "_maxnumnetworks_" + str(maxnumnetworks) + "_batchsize_" + str(
                        batchsize) + "_losses.csv", "a") as file:
                    writer = csv.writer(file, delimiter=",")
                    writer.writerow(errors)
                model.save(networkpath + "/" + domain + "_" + str(numofsavednetworks) + ".h5")
                numofsavednetworks += 1
        while numofsavednetworks <= maxnumnetworks:
            model.fit(x=nn_input_train, y=nn_output_train, batch_size=batchsize, epochs=1, verbose=0)
            vallosses = model.evaluate(x=nn_input_val, y=nn_output_val, batch_size=batchsize, verbose=0)
            huber = np.round(vallosses[1], decimals=4)
            l1 = np.round(vallosses[2], decimals=4)
            l_inf = np.round(vallosses[3], decimals=4)
            if l_inf + secondepochtolerance <= latestlinf:
                print("#" + str(numofsavednetworks) + " of " + str(maxnumnetworks) + " : latest:" + str(
                    latestlinf) + "_current:" + str(l_inf))
                latestlinf = l_inf
                errors = [str(numofsavednetworks), str(huber), str(l1), str(l_inf)]
                with open(networkpath + "/" + domain + "_maxnumnetworks_" + str(maxnumnetworks) + "_batchsize_" + str(
                        batchsize) + "_losses.csv", "a") as file:
                    writer = csv.writer(file, delimiter=",")
                    writer.writerow(errors)
                model.save(networkpath + "/" + domain + "_" + str(numofsavednetworks) + ".h5")
                numofsavednetworks += 1

    elif domain == "IIGS5T4":
        epoch = 0
        while numofsavednetworks <= maxnumnetworks - 5:
            model.fit(x=nn_input_train, y=nn_output_train, batch_size=batchsize, epochs=10, verbose=0)
            epoch += 10
            print("Epoch: ", epoch)
            vallosses = model.evaluate(x=nn_input_val, y=nn_output_val, batch_size=batchsize, verbose=0)
            huber = np.round(vallosses[1], decimals=4)
            l1 = np.round(vallosses[2], decimals=4)
            l_inf = np.round(vallosses[3], decimals=4)
            if l_inf + firstepochtolerance <= latestlinf:
                print("#" + str(numofsavednetworks) + " of " + str(maxnumnetworks) + " : latest:" + str(
                    latestlinf) + "_current:" + str(l_inf))
                latestlinf = l_inf
                errors = [str(numofsavednetworks), str(huber), str(l1), str(l_inf)]
                with open(networkpath + "/" + domain + "_maxnumnetworks_" + str(maxnumnetworks) + "_batchsize_" + str(
                        batchsize) + "_losses.csv", "a") as file:
                    writer = csv.writer(file, delimiter=",")
                    writer.writerow(errors)
                model.save(networkpath + "/" + domain + "_" + str(numofsavednetworks) + ".h5")
                numofsavednetworks += 1
        while numofsavednetworks <= maxnumnetworks:
            model.fit(x=nn_input_train, y=nn_output_train, batch_size=batchsize, epochs=10, verbose=0)
            epoch += 10
            print("Epoch: ", epoch)
            vallosses = model.evaluate(x=nn_input_val, y=nn_output_val, batch_size=batchsize, verbose=0)
            huber = np.round(vallosses[1], decimals=4)
            l1 = np.round(vallosses[2], decimals=4)
            l_inf = np.round(vallosses[3], decimals=4)
            if l_inf + secondepochtolerance <= latestlinf:
                print("#" + str(numofsavednetworks) + " of " + str(maxnumnetworks) + " : latest:" + str(
                    latestlinf) + "_current:" + str(l_inf))
                latestlinf = l_inf
                errors = [str(numofsavednetworks), str(huber), str(l1), str(l_inf)]
                with open(networkpath + "/" + domain + "_maxnumnetworks_" + str(maxnumnetworks) + "_batchsize_" + str(
                        batchsize) + "_losses.csv", "a") as file:
                    writer = csv.writer(file, delimiter=",")
                    writer.writerow(errors)
                model.save(networkpath + "/" + domain + "_" + str(numofsavednetworks) + ".h5")
                numofsavednetworks += 1

    elif domain == "IIGS4T4":
        epoch = 0
        while numofsavednetworks <= maxnumnetworks - 5:
            model.fit(x=nn_input_train, y=nn_output_train, batch_size=batchsize, epochs=10, verbose=0)
            epoch += 10
            print("Epoch: ", epoch)
            vallosses = model.evaluate(x=nn_input_val, y=nn_output_val, batch_size=batchsize, verbose=0)
            huber = np.round(vallosses[1], decimals=4)
            l1 = np.round(vallosses[2], decimals=4)
            l_inf = np.round(vallosses[3], decimals=4)
            if l_inf + firstepochtolerance <= latestlinf:
                print("#" + str(numofsavednetworks) + " of " + str(maxnumnetworks) + " : latest:" + str(
                    latestlinf) + "_current:" + str(l_inf))
                latestlinf = l_inf
                errors = [str(numofsavednetworks), str(huber), str(l1), str(l_inf)]
                with open(networkpath + "/" + domain + "_maxnumnetworks_" + str(maxnumnetworks) + "_batchsize_" + str(
                        batchsize) + "_losses.csv", "a") as file:
                    writer = csv.writer(file, delimiter=",")
                    writer.writerow(errors)
                model.save(networkpath + "/" + domain + "_" + str(numofsavednetworks) + ".h5")
                numofsavednetworks += 1
        while numofsavednetworks <= maxnumnetworks:
            model.fit(x=nn_input_train, y=nn_output_train, batch_size=batchsize, epochs=10, verbose=0)
            epoch += 10
            print("Epoch: ", epoch)
            vallosses = model.evaluate(x=nn_input_val, y=nn_output_val, batch_size=batchsize, verbose=0)
            huber = np.round(vallosses[1], decimals=4)
            l1 = np.round(vallosses[2], decimals=4)
            l_inf = np.round(vallosses[3], decimals=4)
            if l_inf + secondepochtolerance <= latestlinf:
                print("#" + str(numofsavednetworks) + " of " + str(maxnumnetworks) + " : latest:" + str(
                    latestlinf) + "_current:" + str(l_inf))
                latestlinf = l_inf
                errors = [str(numofsavednetworks), str(huber), str(l1), str(l_inf)]
                with open(networkpath + "/" + domain + "_maxnumnetworks_" + str(maxnumnetworks) + "_batchsize_" + str(
                        batchsize) + "_losses.csv", "a") as file:
                    writer = csv.writer(file, delimiter=",")
                    writer.writerow(errors)
                model.save(networkpath + "/" + domain + "_" + str(numofsavednetworks) + ".h5")
                numofsavednetworks += 1

    del model
    K.clear_session()


if __name__ == "__main__":
    main(args)
