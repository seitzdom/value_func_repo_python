#!/usr/bin/python
import sys

sys.path.append('/home/matej/gt/python/value_func_repo_python')
from nn.build_nn import *
import pandas as pd
import numpy as np
from argparse import ArgumentParser
import csv

parser = ArgumentParser()
parser.add_argument("--datapath", type=str)
parser.add_argument("--networkpath", type=str)
parser.add_argument("--domain", type=str)
parser.add_argument("--batchsize", type=int)
parser.add_argument("--MaxBatch", type=int)
parser.add_argument("--epochs", type=int)
parser.add_argument("--rw", type=bool, default=False)

args = parser.parse_args()


def main(args):
    datapath = args.datapath
    networkpath = args.networkpath
    domain = args.domain
    batchsize = args.batchsize
    MaxBatch = args.MaxBatch
    epochs = args.epochs
    reachweighted = args.rw

    nn_input = None
    nn_output = None
    input_shape = None
    output_shape = None
    n_hidden = None
    width_num = None
    seed = None

    if domain == "GP322221":
        print("GP322221")
        actionseq = 3
        ps_num = 27
        input_shape = 2 * actionseq + ps_num
        output_shape = 2 * actionseq
        n_hidden = 2
        width_num = 10
        seed = 1
        nn_input = pd.read_csv(datapath + '/data_poker/input_poker.csv')
        nn_output = pd.read_csv(datapath + '/data_poker/output_poker.csv')


    elif domain == "IIGS5T4":
        print("IIGS5T4")
        actionseq = 20
        ps_num = 5
        input_shape = 2 * actionseq + ps_num
        output_shape = 2 * actionseq
        n_hidden = 2
        width_num = 10
        seed = 1
        nn_input = pd.read_csv(datapath + '/data_iigs5t4/input_gs5_trunk4.csv')
        nn_output = pd.read_csv(datapath + '/data_iigs5t4/output_gs5_trunk4.csv')
        batchsize = 100

    elif domain == "OZ8C3B":
        print("OZ8C3B")
        actionseq = 35
        ps_num = 7
        input_shape = 2 * actionseq + ps_num
        output_shape = 2 * actionseq
        n_hidden = 2
        width_num = 10
        seed = 1
        nn_input = pd.read_csv(datapath + '/data_OZ8CB3/input_osh_c8l1_trunk6_2000.csv')
        nn_output = pd.read_csv(datapath + '/data_OZ8CB3/output_osh_c8l1_trunk6_2000.csv')

    elif domain == "IIGS4T4":
        print("IIGS4T4")
        actionseq = 12
        ps_num = 5
        input_shape = 2 * actionseq + ps_num
        output_shape = 2 * actionseq
        n_hidden = 2
        width_num = 10
        seed = 1
        nn_input = pd.read_csv(datapath + '/IIGS4T4_random_input.csv')
        nn_output = pd.read_csv(datapath + '/IIGS4T4_random_output.csv')

    if reachweighted:
        model, input_tensor = build_fnn_dynamic_loss(input_shape, output_shape, n_hidden, width_num, "relu", seed)
        metrics = [reach_weighted_huber(input_tensor, ps_num), reach_weighted_l1(input_tensor, ps_num),
                   reach_weighted_linf(input_tensor, ps_num)]
        loss = reach_weighted_huber(input_tensor, ps_num)
        model.compile(optimizer=keras.optimizers.Adam(0.001), loss=loss, metrics=metrics)
    else:
        model = build_fnn(input_shape, output_shape, n_hidden, width_num, "relu", seed)
        model.compile(optimizer=keras.optimizers.Adam(0.001), loss=huber_loss,
                      metrics=[huber_loss, l1_loss, linf_loss, l0_loss])

    if domain == "IIGS4T4":
        nn_input_train = nn_input.iloc[:-200, :]
        nn_output_train = nn_output.iloc[:-200, :]

        nn_input_val = nn_input.iloc[-200:, :]
        nn_output_val = nn_output.iloc[-200:, :]

    else:
        nn_input_train = nn_input.iloc[:-1000, :]
        nn_output_train = nn_output.iloc[:-1000, :]

        nn_input_val = nn_input.iloc[-1000:, :]
        nn_output_val = nn_output.iloc[-1000:, :]

    columns = ["#", "huber", "l1", "linf"]

    with open(
            networkpath + "/" + domain + "_MaxBatch_" + str(MaxBatch) + "_batchsize_" + str(batchsize) + "_losses.csv",
            "w") as file:
        writer = csv.writer(file, delimiter=",")
        writer.writerow(columns)

    for batch in range(batchsize, MaxBatch, batchsize):
        model.train_on_batch(x=nn_input_train.iloc[batch - batchsize:batch, :],
                             y=nn_output_train.iloc[batch - batchsize:batch, :])
        vallosses = model.evaluate(x=nn_input_val, y=nn_output_val, batch_size=nn_input_val.shape[0])
        huber = np.round(vallosses[1], decimals=4)
        l1 = np.round(vallosses[2], decimals=4)
        l_inf = np.round(vallosses[3], decimals=4)
        errors = [str(batch), str(huber), str(l1), str(l_inf)]
        with open(networkpath + "/" + domain + "_MaxBatch_" + str(MaxBatch) + "_batchsize_" + str(
                batchsize) + "_losses.csv", "a") as file:
            writer = csv.writer(file, delimiter=",")
            writer.writerow(errors)
        model.save(networkpath + "/" + domain + "_" + str(batch) + ".h5")
    for epoch in range(epochs):
        model.fit(x=nn_input_train, y=nn_output_train, batch_size=batchsize, epochs=1, verbose=0)
        if epoch % 5 == 0:
            vallosses = model.evaluate(x=nn_input_val, y=nn_output_val, batch_size=nn_input_val.shape[0])
            huber = np.round(vallosses[1], decimals=4)
            l1 = np.round(vallosses[2], decimals=4)
            l_inf = np.round(vallosses[3], decimals=4)
            errors = [str(MaxBatch + epoch), str(huber), str(l1), str(l_inf)]
            with open(networkpath + "/" + domain + "_MaxBatch_" + str(MaxBatch) + "_batchsize_" + str(
                    batchsize) + "_losses.csv", "a") as file:
                writer = csv.writer(file, delimiter=",")
                writer.writerow(errors)
            model.save(networkpath + "/" + domain + "_" + str(MaxBatch + epoch) + ".h5")
    del model
    K.clear_session()


if __name__ == "__main__":
    main(args)
