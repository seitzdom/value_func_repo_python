from nn.build_nn import *
import pandas as pd

# if __name__ == "__main__":
# 	actionseq = 3
# 	ps_num = 27
# 	input_shape = 2 * actionseq + ps_num
# 	output_shape = 2 * actionseq
# 	width_num = 10
# 	n_hidden= 3
# 	networklist = ["/home/dominik/Desktop/fullymixed_poker_seeds_nonzero_0_19/",
# 	               "/home/dominik/Desktop/fullymixed_poker_seeds_nonzero_0_39/",
# 	               "/home/dominik/Desktop/fullymixed_poker_seeds_nonzero_1/"]
# 	sparsenesslist= [19,39,100]
#
# 	for i in range(len(networklist)):
# 		nn_input = pd.read_csv(networklist[i] + 'data/input_poker.csv')
# 		nn_output = pd.read_csv(networklist[i] + 'data/output_poker.csv')
# 		model = build_fnn(input_shape, output_shape, n_hidden, width_num, 'relu')
# 		model = train_fnn(model,nn_input,nn_output,huber_loss,500,[])
# 		model.save("nonzero_"+str(sparsenesslist[i])+"_percent.h5")

if __name__ == "__main__":
	actionseq = 3
	ps_num = 27
	input_shape = 2 * actionseq + ps_num
	output_shape = 2 * actionseq
	width_num = 10
	n_hidden = 3

	root = '/home/dominik/Downloads/'

	nn_input = pd.read_csv(root + 'input_poker_longencode.csv')
	nn_output = pd.read_csv(root + 'output_poker_longencode.csv')
	model = build_fnn(input_shape, output_shape, n_hidden, width_num, 'relu')
	model = train_fnn(model, nn_input.iloc[:87 * 100, :], nn_output.iloc[:87 * 100, :], huber_loss, 500, [])
	model.save("nonzero_70_percent.h5")
