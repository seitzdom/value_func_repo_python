from nn.build_nn import *
from nn.loss_functions import *
import keras
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

actionseq = 20
ps_num = 5
input_shape = 2 * actionseq + ps_num
output_shape = 2 * actionseq
n_hidden = 2
width_num = 10

nn_input = pd.read_csv('data/data_iigs5t4/input_gs5_trunk4.csv')
nn_output = pd.read_csv('data/data_iigs5t4/output_gs5_trunk4.csv')

nn_input_train = nn_input.iloc[:-1000, :]
nn_output_train = nn_output.iloc[:-1000, :]

nn_input_val = nn_input.iloc[-1000:, :]
nn_output_val = nn_output.iloc[-1000:, :]

batchsize = 300

model = build_fnn(input_shape, output_shape, n_hidden, width_num, "relu", 12123)
model.compile(optimizer=keras.optimizers.Adam(0.001), loss=huber_loss,
              metrics=[huber_loss, l1_loss, linf_loss, l0_loss])

## compute bias and variance example
def compute_var(y_pred):
  y_pred = np.array(y_pred)
  y_pred_mean= np.mean(y_pred,axis=0)
  acc_var = 0
  for i in range(y_pred.shape[0]):
    acc_var += np.mean(np.abs(y_pred_mean - y_pred[i, :]), axis=0)
  return acc_var / y_pred.shape[0]

def compute_bias(y_pred,y_true):
  y_true = np.array(y_true)
  y_pred = np.array(y_pred)
  y_pred_mean= np.mean(y_pred,axis=0)
  acc_bias = 0
  for i in range(y_pred.shape[0]):
    acc_bias += np.mean(np.abs(y_pred_mean - y_true[i, :]), axis=0)
  return acc_bias / y_pred.shape[0]

def np_mse_loss(y_true, y_pred):
  y_true = np.array(y_true)
  y_pred = np.array(y_pred)
  acc_loss = 0
  for i in range(y_true.shape[0]):
    acc_loss += np.mean(np.square(y_true[i, :] - y_pred[i, :]), axis=0)
  return acc_loss / y_true.shape[0]


def train_plot_bias_variance_eachiter(epochs=50):
  biaslist= []
  varlist=[]
  mselist=[]
  x= []

  for epoch in range(epochs):
    model.fit(x=nn_input_train.iloc[:1000,:], y=nn_output_train.iloc[:1000,:], batch_size=batchsize, epochs=1, verbose=0)
    pred = model.predict(nn_input_val)
    varlist.append(compute_var(pred))
    biaslist.append(compute_bias(pred,nn_output_val)**2)
    mselist.append(np_mse_loss(nn_output_val,pred))
    x.append(epoch)
    plt.plot(x,varlist)
    plt.plot(x,biaslist)
    plt.plot(x,mselist)
    plt.legend(["var","bias**2","mse"])
    plt.draw()
    plt.pause(0.0001)
    plt.clf()

def train_plot_true_vs_pred_eachiter(epochs=50,infosetidx=0):
  y= []
  x = []

  # fig = plt.figure()
  # ax = fig.add_subplot(111)
  # line1, = ax.plot(x, y)

  for epoch in range(epochs):
    model.fit(x=nn_input_train.iloc[:1000,:], y=nn_output_train.iloc[:1000,:], batch_size=batchsize, epochs=1, verbose=0)
    y.append(model.predict(nn_input_val)[12][infosetidx])
    x.append(epoch)
    # line1.set_xdata(x)
    # line1.set_ydata(y)
    # fig.canvas.draw()
    # fig.show()
    # fig.canvas.flush_events()

    plt.plot(x,y,".")
    plt.plot(x,[nn_output_val.iloc[12,infosetidx] for i in range(epoch+1)],".")
    plt.draw()
    plt.pause(0.0001)
    plt.clf()


def main():
  train_plot_bias_variance_eachiter(500)
  # train_plot_true_vs_pred_eachiter(500,3)
if __name__ == "__main__":
  main()





