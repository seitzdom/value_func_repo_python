from nn.build_nn import *
import pandas as pd

if __name__ == "__main__":
	path = "/home/dominik/Desktop/GP_HuberL1Linf/batchcheckpoint/"
	root = '/home/dominik/Downloads/'
	actionseq = 3
	ps_num = 27
	ps_amount = 87
	input_shape = 2 * actionseq + ps_num
	output_shape = 2 * actionseq
	width_num = 10
	epoch_num = 5

	nn_input = pd.read_csv(root + 'input_poker_verynew1.csv')

	nn_output = pd.read_csv(root + 'output_poker_verynew1.csv')

	for LR in [0.001]:

		for seed in range(53, 60):
			randomnet = build_fnn(input_shape, output_shape, width_num, 3, "relu", seed)
			randomnet = train_L1LowLR(randomnet, nn_input, nn_output, LR, path)
			# huber = np.round(randomnet.history.history['val_huber_loss'][-1],decimals=4)
			# l1 = np.round(randomnet.history.history['val_l1_loss'][-1],decimals=4)
			# l_inf = np.round(randomnet.history.history['val_linf_loss'][-1],decimals=4)
			#
			# randomnet.save(path + "GP_Hu_"+ str(huber)+ "_L1_"+ str(l1)+ "_Linf_"+ str(l_inf) + ".h5")
			del randomnet
			K.clear_session()
