from nn.build_nn import *
import pandas as pd

if __name__ == "__main__":

	root = '/home/dominik/Downloads/'
	actionseq = 3
	ps_num = 27
	ps_amount = 87
	input_shape = 2 * actionseq + ps_num
	output_shape = 2 * actionseq
	width_num = 10
	epoch_num = 5

	nn_input = pd.read_csv(root + 'input_poker_verynew1.csv')

	nn_output = pd.read_csv(root + 'output_poker_verynew1.csv')

	for testrun in range(5):

		for desired_loss in [0.12, 0.14, 0.15, 0.17]:
			randomnet = build_fnn(input_shape, output_shape, width_num, 3, "relu", testrun)
			train_fnn(randomnet, nn_input, nn_output, getCertainLoss(desired_loss), 20, [])
			randomnet.save("poker_linf_" + str(desired_loss) + "_testrun_" + str(testrun) + ".h5")
			del randomnet
			K.clear_session()
