import glob
import os
import pandas as pd
import numpy as np
import natsort
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("--domain", type=str)
parser.add_argument("--seedpath", type=str)
parser.add_argument("--datatype",default="random", type=str)
args = parser.parse_args()


def preprocess_seed(seed):
    # TODO Make this function obsolete by improving the way the data arrives from cpp
    seed['ps_idx'] = seed['ps_idx'].str[1:]
    seed['expected_value_p0'] = seed['expected_value_p0'].str[:-1]
    seed[['reach_prob1', 'expected_value_p0', 'chance_prob']] = seed[
        ['reach_prob1', 'expected_value_p0', 'chance_prob']].astype(float)
    cfv0 = seed['chance_prob'] * seed['reach_prob1'] * seed['expected_value_p0']
    seed['cfv0'] = cfv0
    cfv1 = seed['chance_prob'] * seed['reach_prob0'] * seed['expected_value_p0'] * (-1)
    seed['cfv1'] = cfv1
    return seed


def SeedPathToNNInput(path, domain, datatype):
    numPossibleHandsP0 = None
    numPossibleHandsP1 = None
    numPublicFeatures = None

    if domain == "GP322221":
        numPossibleHandsP0 = 3
        numPossibleHandsP1 = numPossibleHandsP0
        numPublicFeatures = 27

    elif domain == "IIGS5T4":
        numPossibleHandsP0 = 20
        numPossibleHandsP1 = numPossibleHandsP0
        numPublicFeatures = 5

    elif domain == "IIGS3T2":
        numPossibleHandsP0 = 3
        numPossibleHandsP1 = numPossibleHandsP0
        numPublicFeatures = 3

    elif domain == "IIGS4T2":
        numPossibleHandsP0 = 4
        numPossibleHandsP1 = numPossibleHandsP0
        numPublicFeatures = 3

    elif domain == "IIGS4T4":
        numPossibleHandsP0 = 12
        numPossibleHandsP1 = numPossibleHandsP0
        numPublicFeatures = 5

    elif domain == "IIGS4T6":
        numPossibleHandsP0 = 24
        numPossibleHandsP1 = numPossibleHandsP0
        numPublicFeatures = 9

    elif domain == "IIGS3T2":
        numPossibleHandsP0 = 3
        numPossibleHandsP1 = numPossibleHandsP0
        numPublicFeatures = 3

    elif domain == "IIGS3T4":
        numPossibleHandsP0 = 6
        numPossibleHandsP1 = numPossibleHandsP0
        numPublicFeatures = 6

    elif domain == "OZ8C3B":
        numPossibleHandsP0 = 35
        numPossibleHandsP1 = numPossibleHandsP0
        numPublicFeatures = 7

    elif domain == "KS4x3":
        numPossibleHandsP0 = 36
        numPossibleHandsP1 = 10
        numPublicFeatures = 26

    print(domain)

    seed_indices_cols = ['ps_idx', 'infoset_idx', 'auginfoset_idx', 'history_idx', 'actionseq_p0', 'actionseq_p1']
    seed_ps_feature_cols = ['ps_feature_' + str(i) for i in range(numPublicFeatures)]
    seed_value_cols = ['reach_prob0', 'reach_prob1', 'chance_prob', 'expected_value_p0']
    seed_columns = seed_indices_cols + seed_ps_feature_cols + seed_value_cols

    nn_input = pd.DataFrame(np.zeros((1,numPublicFeatures + numPossibleHandsP0 + numPossibleHandsP1)))
    nn_output = pd.DataFrame(np.zeros((1, numPossibleHandsP0 + numPossibleHandsP1)))

    filelist = ReadSeedsFromPath(path)

    firstseed = pd.read_csv(filelist[0], names=seed_columns)

    PubStateIdxList = getListOfPossiblePubStates(firstseed)

    for f in filelist:

        seed = pd.read_csv(f)
        seed.columns = seed_columns
        seed = preprocess_seed(seed)
        seed[seed_ps_feature_cols] = seed[seed_ps_feature_cols].astype(int)
        seed[seed_indices_cols] = seed[seed_indices_cols].astype(int)
        seed[seed_value_cols] = seed[seed_value_cols].astype(float)

        for pubStateIdx in PubStateIdxList:
            seed_filtered_by_pubState = filter_by_certain_pubState(seed, pubStateIdx)
            nn_input = np.concatenate(
                (nn_input, get_input_row(seed_filtered_by_pubState, numPossibleHandsP0,numPossibleHandsP1, numPublicFeatures)))
            nn_output = np.concatenate((nn_output, get_target_row(seed_filtered_by_pubState, numPossibleHandsP0,numPossibleHandsP1)))

    nn_input = nn_input[1:]
    nn_output = nn_output[1:]

    pd.DataFrame(nn_input).to_csv(path + "/" + domain + "_" + datatype + "_input.csv", index=False)

    pd.DataFrame(nn_output).to_csv(path + "/" + domain + "_" + datatype + "_output.csv", index=False)


def ReadSeedsFromPath(file_directory):
    assert(os.path.isdir(file_directory))
    filelst = [f for f in glob.glob(file_directory + "*.csv", recursive=True)]
    filelst = natsort.natsorted(filelst)
    assert(len(filelst)!=0)
    return filelst


def getListOfPossiblePubStates(seed):
    num_ps = len(seed['ps_idx'].unique())

    return [i for i in range(num_ps)]


def filter_by_certain_pubState(seed, ps_idx):
    return seed[seed['ps_idx'] == ps_idx]


def get_input_row(seed_filtered_by_pubstate, numPossibleHandsP0, numPossibleHandsP1, numPublicFeatures):
    """

    :param seed_filtered_by_pubstate: a seed filtered by a particular public state
    :param num_possible_hands: number of possible HANDS (actionseq in some games/private cards in poker etc)
    :param numPublicFeatures: number of features (columns) describing the public state
    :return: returns a vector of len publicstate_len+2*numactionseq with ranges for each (legal) infoset in that PS
    """
    ## numIndexCols refers to the number of columns of a SEED associated with the indices of pubstats,infosets etc.
    numIndexCols = 6
    ps_ranges_vector = np.zeros((1, numPossibleHandsP0+numPossibleHandsP1))
    actionseq_array = seed_filtered_by_pubstate['actionseq_p0'].unique()
    actionseq_array_p2 = seed_filtered_by_pubstate['actionseq_p1'].unique()

    for actionseq in actionseq_array:
        ps_ranges_vector[0][actionseq] = \
            seed_filtered_by_pubstate.loc[seed_filtered_by_pubstate['actionseq_p0'] == actionseq]['reach_prob0'].iloc[0]
    for actionseq in actionseq_array_p2:
        ps_ranges_vector[0][actionseq + numPossibleHandsP0] = \
            seed_filtered_by_pubstate.loc[seed_filtered_by_pubstate['actionseq_p1'] == actionseq]['reach_prob1'].iloc[0]
    ps_features_vector = np.zeros((1, 1 * numPublicFeatures))
    ps_idx_lst = []
    for i in range(numIndexCols, numIndexCols + numPublicFeatures):
        ps_idx_lst.append(i)
    ps_array = seed_filtered_by_pubstate[seed_filtered_by_pubstate.columns[ps_idx_lst]].values
    for j in range(len(ps_array[0])):
        ps_features_vector[0][j] = ps_array[0][j]
    return np.concatenate((ps_features_vector, ps_ranges_vector), axis=1)


def get_target_row(seed_filtered_by_pubstate, numPossibleHandsP0,numPossibleHandsP1):
    ps_cfv_auginfosets = np.zeros((1, numPossibleHandsP0+numPossibleHandsP1))
    actionseq_array_p0 = seed_filtered_by_pubstate['actionseq_p0'].unique()
    actionseq_array_p1 = seed_filtered_by_pubstate['actionseq_p1'].unique()
    for actionseq in actionseq_array_p0:
        ps_cfv_auginfosets[0][actionseq] = \
            seed_filtered_by_pubstate.loc[seed_filtered_by_pubstate['actionseq_p0'] == actionseq]['cfv0'].sum()
    for actionseq in actionseq_array_p1:
        ps_cfv_auginfosets[0][actionseq + numPossibleHandsP0] = \
            seed_filtered_by_pubstate.loc[seed_filtered_by_pubstate['actionseq_p1'] == actionseq]['cfv1'].sum()
    return ps_cfv_auginfosets


def main(args):
    SeedPathToNNInput(args.seedpath,args.domain,args.datatype)


if __name__ == "__main__":
    # call by python3 -m process_data.SeedsToNNInput --seedpath=/home/dominik/Desktop/cpp_seeds/ --domain=KS4x3 --datatype=random
    main(args)
